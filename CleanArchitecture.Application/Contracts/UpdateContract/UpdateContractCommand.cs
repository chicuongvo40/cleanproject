﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Contracts.UpdateContract
{
    public class UpdateContractCommand : IRequest<Unit>, ICommand
    {

        public int Id { get; set; }

        public int CustomerId { get; set; }

        public string ContractType { get; set; }

        public string TermsAndConditions { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public DateTime LastEditedTime { get; set; }

    }
}
