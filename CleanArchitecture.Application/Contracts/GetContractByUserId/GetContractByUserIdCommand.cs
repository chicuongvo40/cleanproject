﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractByUserId
{
    public class GetContractByUserIdCommand : IRequest<List<ContractDTO>>, ICommand
    {
        public int CustomerId { get; set; }
    }
}
