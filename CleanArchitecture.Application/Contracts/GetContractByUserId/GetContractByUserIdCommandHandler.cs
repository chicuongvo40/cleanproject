﻿using AutoMapper;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.GetContractByUserId
{
    public class GetContractByUserIdCommandHandler : IRequestHandler<GetContractByUserIdCommand, List<ContractDTO>>
    {
        private readonly IContractRepositories _userRepository;
        private readonly IMapper _mapper;

        public GetContractByUserIdCommandHandler(IContractRepositories userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<List<ContractDTO>> Handle(GetContractByUserIdCommand request, CancellationToken cancellationToken)
        {
            var roleId = request.CustomerId;
            var users = await _userRepository.FindByCustomerIdAsync(roleId, cancellationToken);

            if (users != null && users.Count() > 0)
            {
                var userDTOs = _mapper.Map<List<ContractDTO>>(users);
                return userDTOs;
            }

            throw new NotFoundException($"No users found with Role ID {roleId}");
        }
    }
}
