﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;


namespace CleanArchitecture.Application.Contracts.CreateContract
{
    public class CreateContractCommandHandler : IRequestHandler<CreateContractCommand, Contract>
    {
        private readonly IContractRepositories _contractRepository;
        public CreateContractCommandHandler(IContractRepositories contractRepository)
        {
            _contractRepository = contractRepository;
        }
        public async Task<Contract> Handle(CreateContractCommand request, CancellationToken cancellationToken)
        {
            var _contract = new Contract(
              request.CustomerId,
               request.ContractType,
               request.TermsAndConditions,
               request.TimeStart,
               request.TimeEnd,
               request.LastEditedTime
                );
            //await _contractRepository.CreateContractSP(_contract);
            _contractRepository.Add(_contract);
            await _contractRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _contract;
        }
    }
}
