﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Contracts.CreateContract
{
    public class CreateContractCommand : IRequest<Contract>, ICommand
    {

        public int CustomerId { get; set; }

        public string ContractType { get; set; }

        public string TermsAndConditions { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public DateTime LastEditedTime { get; set; }
    }
}
