﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Contracts.DeleteContract
{
    public class DeleteContractCommand : IRequest<Unit>, ICommand
    {
        public DeleteContractCommand(int contractid)
        {
            Id = contractid;
        }
        public int Id { get; set; }
    }
}
