﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Contracts.DeleteContract
{
    public class DeleteContractCommandHandler : IRequestHandler<DeleteContractCommand, Unit>
    {
        private readonly IContractRepositories _contractRepository;

        public DeleteContractCommandHandler(IContractRepositories contractRepository)
        {
            _contractRepository = contractRepository;
        }
        public async Task<Unit> Handle(DeleteContractCommand request, CancellationToken cancellationToken)
        {
            //await _contractRepository.DeleteContractSP(request.Id); 
            var contract = await _contractRepository.FindByIdAsync(request.Id, cancellationToken);

            if (contract != null)
            {
                _contractRepository.Remove(contract);
                await _contractRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}