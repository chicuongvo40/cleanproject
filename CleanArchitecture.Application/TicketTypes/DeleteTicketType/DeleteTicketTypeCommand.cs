﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.DeleteTicketType
{
    public class DeleteTicketTypeCommand : IRequest<Unit>, ICommand
    {
        public DeleteTicketTypeCommand(int typeId)
        {
            Id = typeId;
        }
        public int Id { get; set; }
    }
}

