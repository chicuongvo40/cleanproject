﻿using CleanArchitecture.Application.Schedules.DeleteSchedule;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.DeleteTicketType
{
    public class DeleteTikcetTypeCommandHandler : IRequestHandler<DeleteTicketTypeCommand, Unit>
    {
        private readonly ITicketTypeRepositories _typeRepository;

        public DeleteTikcetTypeCommandHandler(ITicketTypeRepositories typeRepository)
        {
            _typeRepository = typeRepository;
        }
        public async Task<Unit> Handle(DeleteTicketTypeCommand request, CancellationToken cancellationToken)
        {
            //await _braRepository.DeleteBranchSP(request.Id); 
            var type = await _typeRepository.FindByIdAsync(request.Id, cancellationToken);

            if (type != null)
            {
                _typeRepository.Remove(type);
                await _typeRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
