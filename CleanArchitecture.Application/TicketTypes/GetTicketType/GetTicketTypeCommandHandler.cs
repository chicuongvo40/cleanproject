﻿using AutoMapper;
using CleanArchitecture.Application.Schedules.GetSchedule;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.GetTicketType
{
    public class GetTicketTypeCommandHandler : IRequestHandler<GetTicketTypeCommand, PagedResults<TicketTypeDTO>>
    {
        private readonly ITicketTypeRepositories _typeRepository;
        private readonly IMapper _mapper;

        public GetTicketTypeCommandHandler(ITicketTypeRepositories typeRepository, IMapper mapper)
        {
            _typeRepository = typeRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketTypeDTO>> Handle(GetTicketTypeCommand request, CancellationToken cancellationToken)
        {
            //var branch = await _braRepository.GetBranchSP();
            var type = await _typeRepository.FindAllAsync();
            var typeDTOs = _mapper.Map<List<TicketTypeDTO>>(type);
            var op = PageHelper<TicketTypeDTO>.Paging(typeDTOs, request.page, request.size);
            return op;
        }
    }
}
