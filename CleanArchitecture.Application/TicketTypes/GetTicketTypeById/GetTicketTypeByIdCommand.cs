﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.GetTicketTypeById
{
    public class GetTicketTypeByIdCommand : IRequest<TicketTypeDTO>, ICommand
    {
        public int TypeId { get; set; }
    }
}

