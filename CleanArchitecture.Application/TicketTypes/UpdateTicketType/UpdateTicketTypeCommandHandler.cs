﻿using CleanArchitecture.Application.Schedules.UpdateSchedule;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.UpdateTicketType
{
    public class UpdateTicketTypeCommandHandler : IRequestHandler<UpdateTicketTypeCommand, Unit>
    {
        private readonly ITicketTypeRepositories _typeRepository;
        public UpdateTicketTypeCommandHandler(ITicketTypeRepositories typeRepository)
        {
            _typeRepository = typeRepository;
        }
        public async Task<Unit> Handle(UpdateTicketTypeCommand request, CancellationToken cancellationToken)
        {
            var bra = new TicketType();
            bra.Id = request.Id;
            bra.TicketTypeName = request.TicketTypeName;
            bra.KpiDuration = request.KpiDuration;
            //await _braRepository.UpdateBranchSP(bra);
            _typeRepository.Update(bra);
            await _typeRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
