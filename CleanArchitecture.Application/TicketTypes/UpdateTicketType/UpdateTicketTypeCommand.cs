﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketTypes.UpdateTicketType
{
    public class UpdateTicketTypeCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public string TicketTypeName { get; set; }

        public string KpiDuration { get; set; }
    }
}
