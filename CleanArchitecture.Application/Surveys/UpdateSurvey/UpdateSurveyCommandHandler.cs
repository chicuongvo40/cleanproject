﻿using CleanArchitecture.Application.OdsService.UpdateOds;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.UpdateSurvey
{
    public class UpdateSurveyCommandHandler : IRequestHandler<UpdateSurveyCommand, Unit>
    {
        private readonly ISurveyRepositories _surveyRepository;
        public UpdateSurveyCommandHandler(ISurveyRepositories surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }
        public async Task<Unit> Handle(UpdateSurveyCommand request, CancellationToken cancellationToken)
        {
            var ods = new Domain.Entities.Survey();
            ods.Id = request.Id;
            ods.SatisfactionRating = request.SatisfactionRating;
            ods.Comment = request.Comment;
            ods.SurveyDate = request.SurveyDate;
            ods.CustomerId = request.CustomerId;
            _surveyRepository.Update(ods);
            await _surveyRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
