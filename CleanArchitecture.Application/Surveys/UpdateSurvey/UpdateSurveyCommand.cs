﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Survey.UpdateSurvey
{
    public class UpdateSurveyCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }
        public string SatisfactionRating { get; set; }

        public string Comment { get; set; }

        public DateTime SurveyDate { get; set; }

        public int CustomerId { get; set; }
    }
}
