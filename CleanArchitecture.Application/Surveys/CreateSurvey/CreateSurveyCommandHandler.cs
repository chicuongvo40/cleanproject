﻿using CleanArchitecture.Application.OdsService.CreateOds;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.CreateSurvey
{
    public class CreateSurveyCommandHandler : IRequestHandler<CreateSurveyCommand, Domain.Entities.Survey>
    {
        private readonly ISurveyRepositories _surveyRepository;

        public CreateSurveyCommandHandler(ISurveyRepositories surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }
        public async Task<Domain.Entities.Survey> Handle(CreateSurveyCommand request, CancellationToken cancellationToken)
        {
            var ods = new Domain.Entities.Survey(
                request.SatisfactionRating,
                request.Comment,
                request.SurveyDate,
                request.CustomerId
                );
            _surveyRepository.Add(ods);
            await _surveyRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return ods;
        }
    }
}
