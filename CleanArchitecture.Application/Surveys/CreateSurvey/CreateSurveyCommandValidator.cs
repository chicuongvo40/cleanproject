﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.CreateSurvey
{
    public class CreateSurveyCommandValidator : AbstractValidator<CreateSurveyCommand>
    {
        public CreateSurveyCommandValidator()
        {
            RuleFor(command => command.SatisfactionRating)
                .NotEmpty().WithMessage("Satisfaction rating is required")
                .MaximumLength(50).WithMessage("Satisfaction rating must not exceed 50 characters.");

            RuleFor(command => command.Comment)
                .MaximumLength(1000).WithMessage("Comment must not exceed 1000 characters.");

            RuleFor(command => command.SurveyDate)
                .NotEmpty().WithMessage("Survey date is required");
        }
    }
}
