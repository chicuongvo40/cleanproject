﻿using CleanArchitecture.Application.OdsService.DeleteOds;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.DeleteSurvey
{
    public class DeleteSurveyCommandHandler : IRequestHandler<DeleteSurveyCommand, Unit>
    {
        private readonly ISurveyRepositories _surveyRepository;
        public DeleteSurveyCommandHandler(ISurveyRepositories surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }
        public async Task<Unit> Handle(DeleteSurveyCommand request, CancellationToken cancellationToken)
        {
            var ods = await _surveyRepository.FindByIdAsync(request.Id, cancellationToken);

            if (ods != null)
            {
                _surveyRepository.Remove(ods);
                await _surveyRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
