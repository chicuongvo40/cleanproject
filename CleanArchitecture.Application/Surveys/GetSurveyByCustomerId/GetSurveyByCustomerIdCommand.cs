﻿using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Survey.GetSurveyById
{
    public class GetSurveyByIdCommand : IRequest<SurveyDTO>, ICommand
    {
        public int CustomerId { get; set; }
    }
}
