﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.GetSurveyById
{
    public class GetSurveyByCustomerIdCommandHandler : IRequestHandler<GetSurveyByIdCommand, SurveyDTO>
    {
        private readonly ISurveyRepositories _surveyRepo;
        private readonly IMapper _mapper;
        public GetSurveyByCustomerIdCommandHandler(ISurveyRepositories surveyRepo, IMapper mapper)
        {
            _surveyRepo = surveyRepo;
            _mapper = mapper;
        }
        public async Task<SurveyDTO> Handle(GetSurveyByIdCommand request, CancellationToken cancellationToken)
        {
            var customerId = request.CustomerId;

            var survey = await _surveyRepo.GetSurveyByCustomerIdSP(customerId, cancellationToken);

            if (survey != null)
            {
                var surveyDTO = _mapper.Map<SurveyDTO>(survey.First());
                return surveyDTO;
            }
            throw new NotFoundException($"Survey with ID {customerId} not found");
        }
    }
}
