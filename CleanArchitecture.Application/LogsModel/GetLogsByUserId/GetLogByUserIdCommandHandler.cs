﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;

namespace CleanArchitecture.Application.LogsModel.GetLogsById
{
    public class GetLogByUserIdCommandHandler : IRequestHandler<GetLogByUserIdCommand, LogDTO>
    {
        private readonly ILogRepository _logRepository;
        private readonly IMapper _mapper;
        public GetLogByUserIdCommandHandler(ILogRepository logRepository, IMapper mapper)
        {
            _logRepository = logRepository;
            _mapper = mapper;
        }
        public async Task<LogDTO> Handle(GetLogByUserIdCommand request, CancellationToken cancellationToken)
        {
            var userId = request.UserId;

            var log = await _logRepository.GetLogByUserIdSP(userId, cancellationToken);

            if (log != null)
            {
                var logDTO = _mapper.Map<LogDTO>(log.First());
                return logDTO;
            }
            throw new NotFoundException($"Log with ID {userId} not found");
        }
    }
}
