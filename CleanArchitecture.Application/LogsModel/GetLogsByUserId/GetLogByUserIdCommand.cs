﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;

namespace CleanArchitecture.Application.LogsModel.GetLogsById
{
    public class GetLogByUserIdCommand : IRequest<LogDTO>, ICommand
    {
        public int UserId { get; set; }
    }
}
