﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.SearchTimeLogs
{
    public class SearchaTimeLogCommand : IRequest<LogDTO>, ICommand
    {
        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }
    }
}
