﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;


namespace CleanArchitecture.Application.LogsModel.SearchTimeLogs
{
    public class SearchTimeLogCommandHandler : IRequestHandler<SearchaTimeLogCommand, LogDTO>
    {
        private readonly ILogRepository _logRepository;
        private readonly IMapper _mapper;

        public SearchTimeLogCommandHandler(ILogRepository logRepository, IMapper mapper)
        {
            _logRepository = logRepository;
            _mapper = mapper;
        }

        public async Task<LogDTO> Handle(SearchaTimeLogCommand request, CancellationToken cancellationToken)
        {

            var customer = await _logRepository.SearchTineLogs(request.TimeStart,request.TimeEnd, cancellationToken);

            if (customer != null)
            {
                var customerDTO = _mapper.Map<LogDTO>(customer.First());
                return customerDTO;
            }
            throw new NotFoundException($"Customer with Time not found");
        }
    }
}
