﻿using CleanArchitecture.Application.Branchs.UpdateBrach;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.UpdateLogs
{
    public class UpdateLogCommandHandler : IRequestHandler<UpdateLogCommand, Unit>
    {
        private readonly ILogRepository _logRepository;
        public UpdateLogCommandHandler(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public async Task<Unit> Handle(UpdateLogCommand request, CancellationToken cancellationToken)
        {
            var log = new Log();
            log.Id = request.Id;
            log.TicketId = request.TicketId;
            log.Note = request.Note;
            log.CreatedDate = request.CreatedDate;
            log.IsAssigned = request.IsAssigned;
            log.AssignedTo = request.AssignedTo;
            log.Reason = request.Reason;
            log.TimeStart = request.TimeStart;
            log.TimeEnd = request.TimeEnd;
            log.UserId = request.UserId;
            //await _logRepository.UpdateLogSP(bra);
            _logRepository.Update(log);
            await _logRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}