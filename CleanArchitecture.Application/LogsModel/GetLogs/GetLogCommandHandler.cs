﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.GetLogs
{
    public class GetLogCommandHandler : IRequestHandler<GetLogCommand, PagedResults<LogDTO>>
    {
        private readonly ILogRepository _logRepository;
        private readonly IMapper _mapper;

        public GetLogCommandHandler(ILogRepository logRepository, IMapper mapper)
        {
            _logRepository = logRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<LogDTO>> Handle(GetLogCommand request, CancellationToken cancellationToken)
        {
            var log = await _logRepository.GetLogSP();
            var logDTOs = _mapper.Map<List<LogDTO>>(log);
            var op = PageHelper<LogDTO>.Paging(logDTOs, request.page, request.size);
            return op;
        }
    }
}
