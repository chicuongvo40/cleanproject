﻿using AutoMapper;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.GetLogByTicketId
{
    public class GetLogByTicketIdCommandHandler : IRequestHandler<GetLogByTicketIdCommand, List<LogDTO>>
    {
        private readonly ILogRepository _logRepository;
        private readonly IMapper _mapper;

        public GetLogByTicketIdCommandHandler(ILogRepository logRepository, IMapper mapper)
        {
            _logRepository = logRepository;
            _mapper = mapper;
        }

        public async Task<List<LogDTO>> Handle(GetLogByTicketIdCommand request, CancellationToken cancellationToken)
        {
            var roleId = request.TicketId;
            var users = await _logRepository.FindByTicketIdAsync(roleId, cancellationToken);

            if (users != null && users.Count > 0)
            {
                var userDTOs = _mapper.Map<List<LogDTO>>(users);
                return userDTOs;
            }

            throw new NotFoundException($"No users found with Role ID {roleId}");
        }
    }
}
