﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.GetLogByTicketId
{
    public class GetLogByTicketIdCommand : IRequest<List<LogDTO>>, ICommand
    {
        public int TicketId { get; set; }
    }
}
