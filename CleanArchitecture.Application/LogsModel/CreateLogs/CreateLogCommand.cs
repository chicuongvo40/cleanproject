﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.LogsModel.CreateLogs
{
    public class CreateLogCommand : IRequest<Log>, ICommand
    {
        public int TicketId { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public string IsAssigned { get; set; }

        public string AssignedTo { get; set; }

        public string Reason { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public int UserId { get; set; }
    }
}
