﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.LogsModel.CreateLogs
{
    public class CreateLogCommandValidator : AbstractValidator<CreateLogCommand>
    {
        public CreateLogCommandValidator()
        {
            RuleFor(command => command.Note)
                .MaximumLength(1000).WithMessage("Note must not exceed 1000 characters.");

            RuleFor(command => command.CreatedDate)
                .NotEmpty().WithMessage("Created date is required");

            RuleFor(command => command.IsAssigned)
                .NotEmpty().WithMessage("IsAssigned is required");

            RuleFor(command => command.AssignedTo)
                .MaximumLength(50).WithMessage("AssignedTo must not exceed 50 characters.");

            RuleFor(command => command.Reason)
                .MaximumLength(1000).WithMessage("Reason must not exceed 1000 characters.");

            RuleFor(command => command.TimeStart)
                .NotEmpty().WithMessage("Start time is required");

            RuleFor(command => command.TimeEnd)
                .NotEmpty().WithMessage("End time is required")
                .GreaterThanOrEqualTo(command => command.TimeStart).WithMessage("End time must be greater than or equal to Start time");
        }
    }
}
