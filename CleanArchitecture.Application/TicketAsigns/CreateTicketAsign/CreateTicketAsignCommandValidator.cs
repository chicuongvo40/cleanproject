﻿using CleanArchitecture.Application.TicketAsigns.CreateTicketAsign;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsign.CreateTicketAsign
{
    public class CreateTicketAsignCommandValidator : AbstractValidator<CreateTicketAsignCommand>
    {
        public CreateTicketAsignCommandValidator()
        {
            RuleFor(command => command.TimeStart)
                .NotEmpty().WithMessage("Start time is required");

            RuleFor(command => command.TimeEnd)
                .NotEmpty().WithMessage("End time is required")
                .GreaterThanOrEqualTo(command => command.TimeStart).WithMessage("End time must be greater than or equal to Start time");

            RuleFor(command => command.Status)
                .NotEmpty().WithMessage("Status is required")
                .MaximumLength(50).WithMessage("Status must not exceed 50 characters.");
        }
    }
}
