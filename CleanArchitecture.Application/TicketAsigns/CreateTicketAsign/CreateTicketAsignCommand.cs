﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsigns.CreateTicketAsign
{
    public class CreateTicketAsignCommand : IRequest<Domain.Entities.TicketAsign>, ICommand
    {
        public int TicketId { get; set; }

        public DateTime TimeStart { get; set; }

        public DateTime TimeEnd { get; set; }

        public string Status { get; set; }

        public int UserId { get; set; }
    }
}
