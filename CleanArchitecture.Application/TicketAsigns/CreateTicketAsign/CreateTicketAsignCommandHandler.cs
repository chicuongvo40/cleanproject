﻿using CleanArchitecture.Application.Survey.CreateSurvey;
using CleanArchitecture.Application.TicketAsigns.CreateTicketAsign;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsign.CreateTicketAsign
{
    internal class CreateTicketAsignCommandHandler : IRequestHandler<CreateTicketAsignCommand, Domain.Entities.TicketAsign>
    {
        private readonly ITicketAsignRepositories _ticketAsignRepository;

        public CreateTicketAsignCommandHandler(ITicketAsignRepositories ticketAsignRepository)
        {
            _ticketAsignRepository = ticketAsignRepository;
        }
        public async Task<Domain.Entities.TicketAsign> Handle(CreateTicketAsignCommand request, CancellationToken cancellationToken)
        {
            var ods = new Domain.Entities.TicketAsign(
                request.TicketId,
                request.TimeStart,
                request.TimeEnd,
                request.Status,
                request.UserId
                );
            _ticketAsignRepository.Add(ods);
            await _ticketAsignRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return ods;
        }
    }
}
