﻿using AutoMapper;
using CleanArchitecture.Application.Survey.GetSurveyById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsign.GetTicketAsignById
{
    public class GetTicketAsignByIdCommandHandler : IRequestHandler<GetTicketAsignByIdCommand, TicketAsignDTO>
    {
        private readonly ITicketAsignRepositories _ticketAsignRepo;
        private readonly IMapper _mapper;
        public GetTicketAsignByIdCommandHandler(ITicketAsignRepositories ticketAsignRepo, IMapper mapper)
        {
            _ticketAsignRepo = ticketAsignRepo;
            _mapper = mapper;
        }
        public async Task<TicketAsignDTO> Handle(GetTicketAsignByIdCommand request, CancellationToken cancellationToken)
        {
            var asignId = request.TicketAsignId;

            var asign = await _ticketAsignRepo.GetTicketAsignByIdSP(asignId, cancellationToken);

            if (asign != null)
            {
                var asignIdDTO = _mapper.Map<TicketAsignDTO>(asign.First());
                return asignIdDTO;
            }
            throw new NotFoundException($"TicketAsign with ID {asignId} not found");
        }
    }
}
