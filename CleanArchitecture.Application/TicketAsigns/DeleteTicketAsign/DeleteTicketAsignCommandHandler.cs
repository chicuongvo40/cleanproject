﻿using CleanArchitecture.Application.Survey.DeleteSurvey;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsigns.DeleteTicketAsign
{
    public class DeleteTicketAsignCommandHandler : IRequestHandler<DeleteTicketAsignCommand, Unit>
    {
        private readonly ITicketAsignRepositories _ticketAsignRepository;
        public DeleteTicketAsignCommandHandler(ITicketAsignRepositories ticketAsignRepository)
        {
            _ticketAsignRepository = ticketAsignRepository;
        }
        public async Task<Unit> Handle(DeleteTicketAsignCommand request, CancellationToken cancellationToken)
        {
            var ods = await _ticketAsignRepository.FindByIdAsync(request.Id, cancellationToken);

            if (ods != null)
            {
                _ticketAsignRepository.Remove(ods);
                await _ticketAsignRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
