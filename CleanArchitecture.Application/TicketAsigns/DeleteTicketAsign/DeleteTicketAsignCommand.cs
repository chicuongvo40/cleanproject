﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsigns.DeleteTicketAsign
{
    public class DeleteTicketAsignCommand : IRequest<Unit>, ICommand
    {
        public DeleteTicketAsignCommand(int asignId)
        {
            Id = asignId;
        }
        public int Id { get; set; }
    }
}
