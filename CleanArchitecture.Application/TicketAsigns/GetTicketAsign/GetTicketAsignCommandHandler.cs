﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Survey.GetSurvey
{
    public class GetTicketAsignCommandHandler : IRequestHandler<GetTicketAsignCommand, PagedResults<TicketAsignDTO>>
    {
        private readonly ITicketAsignRepositories _ticketAsignRepository;
        private readonly IMapper _mapper;

        public GetTicketAsignCommandHandler(ITicketAsignRepositories ticketAsignRepository, IMapper mapper)
        {
            _ticketAsignRepository = ticketAsignRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketAsignDTO>> Handle(GetTicketAsignCommand request, CancellationToken cancellationToken)
        {
            var ticketasign = await _ticketAsignRepository.GetTicketAsignSP();
            var ticketasignDTOs = _mapper.Map<List<TicketAsignDTO>>(ticketasign);
            var op = PageHelper<TicketAsignDTO>.Paging(ticketasignDTOs, request.page, request.size);
            return op;
        }
    }
}
