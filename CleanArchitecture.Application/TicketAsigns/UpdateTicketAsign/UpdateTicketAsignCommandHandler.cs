﻿using CleanArchitecture.Application.Survey.UpdateSurvey;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketAsigns.UpdateTicketAsign
{
    internal class UpdateTicketAsignCommandHandler : IRequestHandler<UpdateTicketAsignCommand, Unit>
    {
        private readonly ITicketAsignRepositories _ticketAsignRepository;
        public UpdateTicketAsignCommandHandler(ITicketAsignRepositories ticketAsignRepository)
        {
            _ticketAsignRepository = ticketAsignRepository;
        }
        public async Task<Unit> Handle(UpdateTicketAsignCommand request, CancellationToken cancellationToken)
        {
            var ods = new Domain.Entities.TicketAsign();
            ods.Id = request.Id;
            ods.TicketId = request.TicketId;
            ods.TimeStart = request.TimeStart;
            ods.TimeEnd = request.TimeEnd;
            ods.Status = request.Status;
            ods.UserId = request.UserId;
            _ticketAsignRepository.Update(ods);
            await _ticketAsignRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}

