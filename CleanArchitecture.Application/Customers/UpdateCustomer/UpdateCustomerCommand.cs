﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Customers.UpdateCustomer
{
    public class UpdateCustomerCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }

        public int CustomerLevelId { get; set; }

        public int SourceId { get; set; }

        public int BranchId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string PhoneNumber { get; set; }

        public string Status { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public DateTime DayOfBirth { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime LastEditedTime { get; set; }
        public string Name { get; set; }

    }
}

