﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.UpdateStatusCustomer
{
    public class UpdateStatusCustomerCommandHandler : IRequestHandler<UpdateStatusCustomerCommand, Customer>
    {
        private readonly ICustomerRepositories _userRepository;

        public UpdateStatusCustomerCommandHandler(ICustomerRepositories userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Customer> Handle(UpdateStatusCustomerCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindByIdAsync(request.Id);

            if (user == null)
            {
                throw new Exception("User not found");
            }

            user.Status = "Onl";

            _userRepository.Update(user);

            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return user;
        }
    }
}
