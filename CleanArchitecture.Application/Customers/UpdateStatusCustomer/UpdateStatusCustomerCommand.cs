﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Customers.UpdateStatusCustomer
{
    public class UpdateStatusCustomerCommand : IRequest<Customer>, ICommand
    {
        public int Id { get; set; }
    }
}
