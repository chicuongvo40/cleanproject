﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Customers.DeleteCustomer
{
    public class DeleteCustomerCommand : IRequest<Unit>, ICommand
    {

        public DeleteCustomerCommand(int id)
        {
            CustomerId = id;
        }

        public int CustomerId { get; set; }
    }
}

