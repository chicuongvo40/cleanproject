﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Customers.GetCustomerById
{
    public class GetCustomerByIdCommand : IRequest<CustomerDTO>, ICommand
    {
        public int CustomerId { get; set; }
    }
}
