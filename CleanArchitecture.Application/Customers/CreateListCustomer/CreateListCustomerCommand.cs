﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Customers.CreateListCustomer
{
    public class CreateListCustomerCommand : IRequest<List<Customer>>, ICommand
    {
        public List<Customer> List { get; set; }

    }
}
