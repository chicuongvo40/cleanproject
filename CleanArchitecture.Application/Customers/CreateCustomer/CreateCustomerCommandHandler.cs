﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.CreateCustomer
{
    public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, Customer>
    {
        private readonly ICustomerRepositories _cusRepository;


        public CreateCustomerCommandHandler(ICustomerRepositories cusRepository)
        {
            _cusRepository = cusRepository;
        }
        public async Task<Customer> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = new Customer(
               request.CustomerLevelId,
               request.SourceId,
               request.BranchId,
               request.LastName,
               request.FirstName,
               request.PhoneNumber,
               request.Status,
               request.Gender,
               request.Address,
               request.DayOfBirth,
               request.DateCreated,
               request.LastEditedTime,
               request.Name
                );
            // await _cusRepository.CreateCustomerSP(customer);
            _cusRepository.Add(customer);
            await _cusRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return customer;
        }
    }
}

