﻿using AutoMapper;
using CleanArchitecture.Application.Customers.GetCustomerById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.SearchCustomer
{
    public class SearchCustomerCommandHandler : IRequestHandler<SearchCustomerCommand, CustomerDTO>
    {
        private readonly ICustomerRepositories _cusRepository;
        private readonly IMapper _mapper;

        public SearchCustomerCommandHandler(ICustomerRepositories cusRepository, IMapper mapper)
        {
            _cusRepository = cusRepository;
            _mapper = mapper;
        }

        public async Task<CustomerDTO> Handle(SearchCustomerCommand request, CancellationToken cancellationToken)
        {
            var customername = request.CustomerName;

            var customer = await _cusRepository.SearchCustomerSP(customername, cancellationToken);

            if (customer != null)
            {
                var customerDTO = _mapper.Map<CustomerDTO>(customer.First());
                return customerDTO;
            }
            throw new NotFoundException($"Customer with Name {customername} not found");
        }
    }
}
