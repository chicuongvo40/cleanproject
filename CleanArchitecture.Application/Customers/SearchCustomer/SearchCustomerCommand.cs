﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.SearchCustomer
{
    public class SearchCustomerCommand : IRequest<CustomerDTO>, ICommand
    {
        public string CustomerName { get; set; }
    }
}
