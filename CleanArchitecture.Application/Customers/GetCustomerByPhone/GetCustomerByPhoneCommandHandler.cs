﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.GetCustomerByPhone
{
    public class GetCustomerByPhoneCommandHandler : IRequestHandler<GetCustomerByPhoneCommand, CustomerDTO>
    {
        private readonly ICustomerRepositories _customerRepository;
        private readonly IMapper _mapper;

        public GetCustomerByPhoneCommandHandler(ICustomerRepositories customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<CustomerDTO> Handle(GetCustomerByPhoneCommand request, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.FindByPhoneNumberAsync(request.PhoneNumber, cancellationToken);

            if (customer != null)
            {
                return _mapper.Map<CustomerDTO>(customer);
            }

            throw new NotFoundException($"Customer with phone number '{request.PhoneNumber}' not found");
        }
    }
}
