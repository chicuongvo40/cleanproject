﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Customers.GetCustomerByPhone
{
    public class GetCustomerByPhoneCommand : IRequest<CustomerDTO>, ICommand
    {
        public string PhoneNumber { get; set; }
    }
}
