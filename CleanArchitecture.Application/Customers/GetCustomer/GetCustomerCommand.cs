﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Customers.GetCustomer
{
    public class GetCustomerCommand : IRequest<PagedResults<CustomerDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
