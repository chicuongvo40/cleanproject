﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region CustomerLevel
            CreateMap<Domain.Entities.CustomerLevel, CustomerLevelDTO>().ReverseMap();
            #endregion

            #region Source
            CreateMap<Source, SourceDTO>().ReverseMap();
            #endregion

            #region TicketTag
            CreateMap<TicketTag, TicketTagDTO>().ReverseMap();
            #endregion

            #region TicketType
            CreateMap<TicketType, TicketTypeDTO>().ReverseMap();
            #endregion

            #region TicketImage
            CreateMap<TicketImage, TicketImageDTO>().ReverseMap();
            #endregion

            #region TicketAsign
            CreateMap<Domain.Entities.TicketAsign, TicketAsignDTO>().ReverseMap();
            #endregion

            #region Customer
            CreateMap<Customer, CustomerDTO>().ReverseMap();
            #endregion

            #region Branch
            CreateMap<Branch, BranchDTO>().ReverseMap();
            #endregion

            #region CallHistory
            CreateMap<CallHistory, CallHistoryDTO>().ReverseMap();
            #endregion

            #region Level
            CreateMap<Level, LevelDTO>().ReverseMap();
            #endregion

            #region Contract
            CreateMap<Contract, ContractDTO>().ReverseMap();
            #endregion

            #region Survey
            CreateMap<Domain.Entities.Survey, SurveyDTO>().ReverseMap();
            #endregion

            #region Ods
            CreateMap<Od, OdsDTO>().ReverseMap();
            #endregion

            #region Ticket
            CreateMap<Ticket, TicketDTO>().ReverseMap();
            #endregion

            #region User
            CreateMap<User, UserDTO>().ReverseMap();
            #endregion

            #region Log
            CreateMap<Log, LogDTO>().ReverseMap();
            #endregion

            #region Schedule
            CreateMap<Schedule, ScheduleDTO>().ReverseMap();
            #endregion
        }
    }
}
