﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.GetSource
{
    public class GetSourceCommand : IRequest<PagedResults<SourceDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
