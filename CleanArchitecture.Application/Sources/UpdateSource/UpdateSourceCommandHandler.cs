﻿using CleanArchitecture.Application.TicketTypes.UpdateTicketType;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.UpdateSource
{
    public class UpdateSourceCommandHandler : IRequestHandler<UpdateSourceCommand, Unit>
    {
        private readonly ISourceRepositories _sourceRepository;
        public UpdateSourceCommandHandler(ISourceRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Unit> Handle(UpdateSourceCommand request, CancellationToken cancellationToken)
        {
            var source = new Source();
            source.Id = request.Id;
            source.SourceName = request.SourceName;
            _sourceRepository.Update(source);
            await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
