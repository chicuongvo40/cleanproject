﻿using CleanArchitecture.Application.TicketTypes.CreateTicketType;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.CreateSource
{
    public class CreateSourceCommandHandler : IRequestHandler<CreateSourceCommand, Source>
    {
        private readonly ISourceRepositories _sourceRepository;
        public CreateSourceCommandHandler(ISourceRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Source> Handle(CreateSourceCommand request, CancellationToken cancellationToken)
        {
            var _source = new Source(
               request.SourceName
                );
            _sourceRepository.Add(_source);
            await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _source;

        }
    }
}
