﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.GetSourceById
{
    public class GetSourceByIdCommand : IRequest<SourceDTO>, ICommand
    {
        public int SourceId { get; set; }
    }
}
