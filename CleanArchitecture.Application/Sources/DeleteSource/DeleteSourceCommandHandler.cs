﻿using CleanArchitecture.Application.TicketTypes.DeleteTicketType;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Sources.DeleteSource
{
    public class DeleteSourceCommandHandler : IRequestHandler<DeleteSourceCommand, Unit>
    {
        private readonly ISourceRepositories _sourceRepository;

        public DeleteSourceCommandHandler(ISourceRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Unit> Handle(DeleteSourceCommand request, CancellationToken cancellationToken)
        {
            var type = await _sourceRepository.FindByIdAsync(request.Id, cancellationToken);

            if (type != null)
            {
                _sourceRepository.Remove(type);
                await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
