﻿using AutoMapper;
using CleanArchitecture.Application.CallHistorys.GetCallHistoryByDay;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportCallHistory
{
    public class GetReportCallHistoryCommandHandler : IRequestHandler<GetReportCallHistoryCommand, object>
    {
        private readonly IReportRepository _callHistoryRepository;
        //private readonly IMapper _mapper;
        public GetReportCallHistoryCommandHandler(IReportRepository callHistoryRepository)
        {
            _callHistoryRepository = callHistoryRepository;
            //_mapper = mapper;
        }

        public async Task<object> Handle(GetReportCallHistoryCommand request, CancellationToken cancellationToken)
        {
           // var dateCall = request.DateCall;

            var callHistoryList = await _callHistoryRepository.GetTotalCallDetail(request.DateStart, request.DateEnd);
             
          return callHistoryList;
        }
    }
}
