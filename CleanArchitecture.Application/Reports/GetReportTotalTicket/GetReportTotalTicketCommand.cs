﻿using CleanArchitecture.Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTotalTicket
{
    public class GetReportTotalTicketCommand : IRequest<object>, ICommand
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
