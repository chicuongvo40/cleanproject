﻿using CleanArchitecture.Application.Reports.GetReportCallHistory;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTotalTicket
{
    public class GetReportToatlTicketCommandHandler : IRequestHandler<GetReportTotalTicketCommand, object>
    {
        private readonly IReportRepository _callHistoryRepository;
        public GetReportToatlTicketCommandHandler(IReportRepository callHistoryRepository)
        {
            _callHistoryRepository = callHistoryRepository;
        }

        public async Task<object> Handle(GetReportTotalTicketCommand request, CancellationToken cancellationToken)
        {

            var ticketList = await _callHistoryRepository.GetTotalTicket(request.DateStart, request.DateEnd);

            return ticketList;
        }
    }
}
