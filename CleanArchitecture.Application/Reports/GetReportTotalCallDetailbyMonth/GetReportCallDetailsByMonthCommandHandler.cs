﻿using CleanArchitecture.Application.Reports.GetReportCallHistory;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Reports.GetReportTotalCallDetailbyMonth
{
    internal class GetReportCallDetailsByMonthCommandHandler : IRequestHandler<GetReportCallDetailsByMonthCommand, object>
    {
        private readonly IReportRepository _callHistoryRepository;
        //private readonly IMapper _mapper;
        public GetReportCallDetailsByMonthCommandHandler(IReportRepository callHistoryRepository)
        {
            _callHistoryRepository = callHistoryRepository;
            //_mapper = mapper;
        }

        public async Task<object> Handle(GetReportCallDetailsByMonthCommand request, CancellationToken cancellationToken)
        {
            // var dateCall = request.DateCall;

            var callHistoryList = await _callHistoryRepository.GetTotalCallDetailByMonth(request.month, request.year);

            return callHistoryList;
        }
    }
}
