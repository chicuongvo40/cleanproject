﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Users.GetAllUser
{
    internal class GetUserCommandHandler : IRequestHandler<GetUserCommand, PagedResults<UserDTO>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public GetUserCommandHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<UserDTO>> Handle(GetUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserSP();
            var userDTOs = _mapper.Map<List<UserDTO>>(user);
            var op = PageHelper<UserDTO>.Paging(userDTOs, request.page, request.size);
            return op;
        }
    }
}
