﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace CleanArchitecture.Application.Users.UpdateUser
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Unit>
    {
        private readonly IUserRepository _userRepository;
        public UpdateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = new User();
            user.Id = request.Id;
            user.BranchId = request.BranchId;
            user.RoleId = request.RoleId;
            user.LastName = request.LastName;
            user.FirstName = request.FirstName;
            user.UserName = request.UserName;
            user.Password = request.Password;
            user.Status = request.Status;
            user.Gender = request.Gender;
            user.Address = request.Address;
            user.DayOfBirth = request.DayOfBirth;
            user.CreatedDate = request.CreatedDate;
            //await _userRepository.UpdateUserSP(bra);
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
