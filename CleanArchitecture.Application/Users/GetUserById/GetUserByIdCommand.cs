﻿using CleanArchitecture.Domain.DTO;
using MediatR;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Users.GetUserById
{
    public class GetUserByIdCommand : IRequest<UserDTO>, ICommand
    {
        public int UserId { get; set; }
    }
}
