﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;

namespace CleanArchitecture.Application.Users.GetUserById
{
    public class GetUserByIdCommandHandler : IRequestHandler<GetUserByIdCommand, UserDTO>
    {
        private readonly IUserRepository _userRepo;
        private readonly IMapper _mapper;
        public GetUserByIdCommandHandler(IUserRepository userRepo, IMapper mapper)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }
        public async Task<UserDTO> Handle(GetUserByIdCommand request, CancellationToken cancellationToken)
        {
            var userId = request.UserId;

            var user = await _userRepo.FindByIdAsync(userId, cancellationToken);

            if (user != null)
            {
                var userDTO = _mapper.Map<UserDTO>(user);
                return userDTO;
            }

            throw new NotFoundException($"User with ID {userId} not found");
        }
    }
}
