﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Users.CreateUser
{
    public class CreateUserCommand : IRequest<User>, ICommand
    {
        public int BranchId { get; set; }

        public int RoleId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Status { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public DateTime DayOfBirth { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
