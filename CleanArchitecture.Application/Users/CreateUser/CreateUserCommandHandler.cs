﻿using CleanArchitecture.Application.Branchs.CreateBranch;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace CleanArchitecture.Application.Users.CreateUser
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, User>
    {
        private readonly IUserRepository _userRepository;
        public CreateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<User> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var _user = new User(
              request.BranchId,
               request.RoleId,
               request.LastName,
               request.FirstName,
               request.UserName,
               request.Password,
               request.Status,
               request.Gender,
               request.Address,
               request.DayOfBirth,
               request.CreatedDate
                );
            // await _userRepository.CreateUserSP(_branch);
            _userRepository.Add(_user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _user;
        }
    }
}
