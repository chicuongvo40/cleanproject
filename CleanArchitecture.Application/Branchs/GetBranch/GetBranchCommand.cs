﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Branchs.GetBranch
{
    public class GetBranchCommand : IRequest<PagedResults<BranchDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
