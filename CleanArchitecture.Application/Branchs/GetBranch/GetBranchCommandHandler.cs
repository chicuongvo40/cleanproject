﻿using AutoMapper;
using CleanArchitecture.Application.Customers.GetCustomer;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Branchs.GetBranch
{
    public class GetBranchCommandHandler : IRequestHandler<GetBranchCommand, PagedResults<BranchDTO>>
    {
        private readonly IBranchRepositories _braRepository;
        private readonly IMapper _mapper;

        public GetBranchCommandHandler(IBranchRepositories braRepository, IMapper mapper)
        {
            _braRepository = braRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<BranchDTO>> Handle(GetBranchCommand request, CancellationToken cancellationToken)
        {
            var branch = await _braRepository.GetBranchSP();
            //var branch = await _braRepository.FindAllAsync();
            var branchDTOs = _mapper.Map<List<BranchDTO>>(branch);
            var op = PageHelper<BranchDTO>.Paging(branchDTOs, request.page, request.size);
            return op;
        }
    }
}
