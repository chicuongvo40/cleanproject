﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Branchs.DeleteBranch
{
    public class DeleteBranchCommand : IRequest<Unit>, ICommand
    {
        public DeleteBranchCommand(int braid)
        {
            Id = braid;
        }
        public int Id { get; set; }
    }
}