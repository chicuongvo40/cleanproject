﻿
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;


namespace CleanArchitecture.Application.Branchs.UpdateBrach
{
    public class UpdateBranchCommandHandler : IRequestHandler<UpdateBranchCommand, Unit>
    {
        private readonly IBranchRepositories _braRepository;
        public UpdateBranchCommandHandler(IBranchRepositories braRepository)
        {
            _braRepository = braRepository;
        }
        public async Task<Unit> Handle(UpdateBranchCommand request, CancellationToken cancellationToken)
        {
            var bra = new Branch();
            bra.Id = request.Id;
            bra.OdsId = request.OdsId;
            bra.Address = request.Address;
            bra.BranchName = request.BranchName;
            //await _braRepository.UpdateBranchSP(bra);
            _braRepository.Update(bra);
            await _braRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
