﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using System.Text.Json.Serialization;

namespace CleanArchitecture.Application.Branchs.UpdateBrach
{
    public class UpdateBranchCommand : IRequest<Unit>, ICommand
    {
       
        public int Id { get; set; }

        public int OdsId { get; set; }

        public string Address { get; set; }

        public string BranchName { get; set; }
    }
}
