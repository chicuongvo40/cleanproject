﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Branchs.CreateBranch
{
    public class CreateBranchCommandValidator : AbstractValidator<CreateBranchCommand>
    {
        public CreateBranchCommandValidator()
        {
            RuleFor(command => command.Address)
                .NotEmpty().WithMessage("Address is required")
                .MaximumLength(100).WithMessage("Address must not exceed 100 characters.");

            RuleFor(command => command.BranchName)
                .NotEmpty().WithMessage("Branch name is required")
                .MaximumLength(50).WithMessage("Branch name must not exceed 50 characters.")
                .Matches("^[a-zA-Z ]+$").WithMessage("Branch name should only contain alphabetic characters.");
        }
    }
}
