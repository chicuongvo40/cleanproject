﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Branchs.GetBranchById
{
    public class GetBranchByIdCommandHandler : IRequestHandler<GetBranchByIdCommand, BranchDTO>
    {
        private readonly IBranchRepositories _branchRepo;
        private readonly IMapper _mapper;
        public GetBranchByIdCommandHandler(IBranchRepositories branchRepo, IMapper mapper)
        {
            _branchRepo = branchRepo;
            _mapper = mapper;
        }
        public async Task<BranchDTO> Handle(GetBranchByIdCommand request, CancellationToken cancellationToken)
        {
            var branchId = request.BranchId;

            var branch = await _branchRepo.FindByIdAsync(branchId, cancellationToken);

            if (branch != null)
            {
                var branchDTO = _mapper.Map<BranchDTO>(branch);
                return branchDTO;
            }
            throw new NotFoundException($"Branch with ID {branchId} not found");
        }
    }
}