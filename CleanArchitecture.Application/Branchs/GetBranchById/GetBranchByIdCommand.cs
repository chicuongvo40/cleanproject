﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Branchs.GetBranchById
{
    public class GetBranchByIdCommand : IRequest<BranchDTO>, ICommand
    {
        public int BranchId { get; set; }
    }
}
