﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.TicketImages.GetTicketImage
{
    public class GetTicketImageCommand : IRequest<PagedResults<TicketImageDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}

