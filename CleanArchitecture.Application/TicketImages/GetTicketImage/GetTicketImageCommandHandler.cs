﻿using AutoMapper;
using CleanArchitecture.Application.Branchs.GetBranch;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.GetTicketImage
{
    public class GetTicketImageCommandHandler : IRequestHandler<GetTicketImageCommand,PagedResults<TicketImageDTO>>
    {
        private readonly ITicketImageRepositories _ImageRepository;
        private readonly IMapper _mapper;


        public GetTicketImageCommandHandler(ITicketImageRepositories ImageRepository, IMapper mapper)
        {
            _ImageRepository = ImageRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketImageDTO>> Handle(GetTicketImageCommand request, CancellationToken cancellationToken)
        {   
            var list = await _ImageRepository.GetTicketImageSP();
            var imageDTOs = _mapper.Map<List<TicketImageDTO>>(list);
            var op = PageHelper<TicketImageDTO>.Paging(imageDTOs, request.page, request.size);
            return op;
        }
    }
}
