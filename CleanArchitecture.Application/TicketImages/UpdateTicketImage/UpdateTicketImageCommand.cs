﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.TicketImages.UpdateTicketImage
{
    public class UpdateTicketImageCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }

        public int? LogId { get; set; }

        public string? ImageUrl { get; set; }
    }
}
