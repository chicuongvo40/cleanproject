﻿using CleanArchitecture.Application.Branchs.UpdateBrach;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.UpdateTicketImage
{
    public class UpdateTicketImageCommandHandler : IRequestHandler<UpdateTicketImageCommand, Unit>
    {
        private readonly ITicketImageRepositories _ImageRepository;
        public UpdateTicketImageCommandHandler(ITicketImageRepositories imageRepository)
        {
            _ImageRepository = imageRepository;
        }
        public async Task<Unit> Handle(UpdateTicketImageCommand request, CancellationToken cancellationToken)
        {
            var image = new TicketImage();
            image.Id = request.Id;
            image.LogId = request.LogId;
            image.ImageUrl = request.ImageUrl;
          _ImageRepository.Update(image);
            await _ImageRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
