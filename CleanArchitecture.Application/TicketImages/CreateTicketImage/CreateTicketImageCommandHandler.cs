﻿using CleanArchitecture.Application.Branchs.CreateBranch;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.CreateTicketImage
{
    public class CreateTicketImageCommandHandler : IRequestHandler<CreateTicketImageCommand, TicketImage>
    {
        private readonly ITicketImageRepositories _ImageRepository;

        public CreateTicketImageCommandHandler(ITicketImageRepositories ImageRepository)
        {
            _ImageRepository = ImageRepository;
        }
        public async Task<TicketImage> Handle(CreateTicketImageCommand request, CancellationToken cancellationToken)
        {
            var image = new TicketImage(
               request.LogId,
               request.ImageUrl
                );
            _ImageRepository.Add(image);
            await _ImageRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return image;
        }
    }
}
