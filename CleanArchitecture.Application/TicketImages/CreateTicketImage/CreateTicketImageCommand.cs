﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.TicketImages.CreateTicketImage
{
    public class CreateTicketImageCommand : IRequest<TicketImage>, ICommand
    {
        public int? LogId { get; set; }

        public string? ImageUrl { get; set; }

    }
}
