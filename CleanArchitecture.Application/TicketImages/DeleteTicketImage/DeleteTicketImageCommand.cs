﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.TicketImages.DeleteTicketImage
{
    public class DeleteTicketImageCommand : IRequest<Unit>, ICommand
    {
        public DeleteTicketImageCommand(int imageId)
        {
            Id = imageId;
        }
        public int Id { get; set; }
    }
}
