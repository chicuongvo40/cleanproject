﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.DeleteTicketImage
{
    public class DeleteTicketImageCommandHandler : IRequestHandler<DeleteTicketImageCommand, Unit>
    {
        private readonly ITicketImageRepositories _ImageRepository;
        public DeleteTicketImageCommandHandler(ITicketImageRepositories ImageRepository)
        {
            _ImageRepository = ImageRepository;
        }
        public async Task<Unit> Handle(DeleteTicketImageCommand request, CancellationToken cancellationToken)
        {
            var image = await _ImageRepository.FindByIdAsync(request.Id, cancellationToken);

            if (image != null)
            {
                _ImageRepository.Remove(image);
                await _ImageRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}

