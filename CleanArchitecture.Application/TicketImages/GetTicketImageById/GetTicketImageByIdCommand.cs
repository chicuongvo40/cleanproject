﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.GetTicketImageById
{
    public class GetTicketImageByIdCommand : IRequest<TicketImageDTO>, ICommand
    {
        public int TicketImageId { get; set; }
    }
}
