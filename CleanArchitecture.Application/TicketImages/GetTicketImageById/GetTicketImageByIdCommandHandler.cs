﻿using AutoMapper;
using CleanArchitecture.Application.TicketAsign.GetTicketAsignById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.TicketImages.GetTicketImageById
{
    public class GetTicketImageByIdCommandHandler : IRequestHandler<GetTicketImageByIdCommand, TicketImageDTO>
    {
        private readonly ITicketImageRepositories _ticketImageRepo;
        private readonly IMapper _mapper;
        public GetTicketImageByIdCommandHandler(ITicketImageRepositories ticketImageRepo, IMapper mapper)
        {
            _ticketImageRepo = ticketImageRepo;
            _mapper = mapper;
        }
        public async Task<TicketImageDTO> Handle(GetTicketImageByIdCommand request, CancellationToken cancellationToken)
        {
            var imageId = request.TicketImageId;

            var image = await _ticketImageRepo.GetTicketImageByIdSP(imageId, cancellationToken);

            if (image != null)
            {
                var imageDTO = _mapper.Map<TicketImageDTO>(image.First());
                return imageDTO;
            }
            throw new NotFoundException($"TicketImage with ID {imageId} not found");
        }
    }
}
