﻿using CleanArchitecture.Application.Tickets.UpdateTicket;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.UpdateLevel
{
    public class UpdateLevelCommandHandler : IRequestHandler<UpdateLevelCommand, Unit>
    {
        private readonly ILevelRepositories _levelRepo;


        public UpdateLevelCommandHandler(ILevelRepositories levelRepo)
        {
            _levelRepo = levelRepo;
        }
        public async Task<Unit> Handle(UpdateLevelCommand request, CancellationToken cancellationToken)
        {
            var level = new Level();
            level.Id = request.Id;
            level.LevelName = request.LevelName;
            //await _levelRepo.UpdateLevelSP(level);
            _levelRepo.Update(level);
            await _levelRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
