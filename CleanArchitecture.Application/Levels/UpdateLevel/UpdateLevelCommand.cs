﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.Entities;

namespace CleanArchitecture.Application.Levels.UpdateLevel
{
    public class UpdateLevelCommand : IRequest<Unit>, ICommand
    {
        public int Id { get; set; }

        public string LevelName { get; set; }

       
    }
}
