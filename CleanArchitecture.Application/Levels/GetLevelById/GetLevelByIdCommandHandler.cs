﻿using AutoMapper;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.GetLevelById
{
    public class GetLevelByIdCommandHandler : IRequestHandler<GetLevelByIdCommand, LevelDTO>
    {
        private readonly ILevelRepositories _levelRepo;
        private readonly IMapper _mapper;
        public GetLevelByIdCommandHandler(ILevelRepositories levelRepo, IMapper mapper)
        {
            _levelRepo = levelRepo;
            _mapper = mapper;
        }

        public async Task<LevelDTO> Handle(GetLevelByIdCommand request, CancellationToken cancellationToken)
        {
            var levelId = request.LevelId;
            var level = await _levelRepo.GetLevelByIdSP(levelId, cancellationToken);

            if (level != null)
            {
                var levelDTO = _mapper.Map<LevelDTO>(level.First());
                return levelDTO;
            }
            throw new NotFoundException($"Level with ID {levelId} not found");
        }
    }
}