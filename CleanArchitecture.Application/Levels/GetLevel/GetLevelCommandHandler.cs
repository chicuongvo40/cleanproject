﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicket;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.GetLevel
{
    public class GetLevelCommandHandler : IRequestHandler<GetLevelCommand, PagedResults<LevelDTO>>
    {
        private readonly ILevelRepositories _levelRepo;
        private readonly IMapper _mapper;
        public GetLevelCommandHandler(ILevelRepositories levelRepo, IMapper mapper)
        {
            _levelRepo = levelRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<LevelDTO>> Handle(GetLevelCommand request, CancellationToken cancellationToken)
        {
            var level = await _levelRepo.GetLevelSP();
            var levelDTO = _mapper.Map<List<LevelDTO>>(level);
            var op = PageHelper<LevelDTO>.Paging(levelDTO, request.page, request.size);
            return op;
        }
    }
}
