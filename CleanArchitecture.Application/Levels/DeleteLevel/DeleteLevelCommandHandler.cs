﻿using CleanArchitecture.Application.Tickets.DeleteTicket;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.DeleteLevel
{
    public class DeleteLevelCommandHandler : IRequestHandler<DeleteLevelCommand, Unit>
    {
        private readonly ILevelRepositories _levelRepo;
        public DeleteLevelCommandHandler(ILevelRepositories levelRepo)
        {
            _levelRepo = levelRepo;
        }
        public async Task<Unit> Handle(DeleteLevelCommand request, CancellationToken cancellationToken)
        {
            //await _levelRepo.DeleteLevelSP(request.Id); 
            var level = await _levelRepo.FindByIdAsync(request.Id, cancellationToken);

            if (level != null)
            {
                _levelRepo.Remove(level);
                await _levelRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
