﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Levels.CreateLevel
{
    public class CreateLevelCommand : IRequest<Level>, ICommand
    {
        public string LevelName { get; set; }       
    }
}
