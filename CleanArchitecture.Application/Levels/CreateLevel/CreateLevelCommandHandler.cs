﻿using CleanArchitecture.Application.Tickets.CreateTicket;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Levels.CreateLevel
{
    public class CreateLevelCommandHandler : IRequestHandler<CreateLevelCommand, Level>
    {
        private readonly ILevelRepositories _levelRepo;
        public CreateLevelCommandHandler(ILevelRepositories levelRepo)
        {
            _levelRepo = levelRepo;
        }
        public async Task<Level> Handle(CreateLevelCommand request, CancellationToken cancellationToken)
        {
            var level = new Level(
               
               request.LevelName
                );
            //await _levelRepo.CreateLevelSP(level);
            _levelRepo.Add(level);
            await _levelRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return level;
        }
    }
}
