﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

public enum CallDirection
{
    [Display(Name = "Inbound")]
    Inbound,
    [Display(Name = "Outbound")]
    Outbound
}
