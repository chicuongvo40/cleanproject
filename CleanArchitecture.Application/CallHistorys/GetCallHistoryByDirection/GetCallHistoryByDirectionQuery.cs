﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
public class GetCallHistoryByDirectionQuery : IRequest<List<CallHistoryDTO>>, ICommand
{
    public string Direction { get; set; }
}
