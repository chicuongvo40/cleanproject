﻿using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CallHistorys.UpdateCallHistory
{
    public class UpdateCallHistoryCommandHandler : IRequestHandler<UpdateCallHistoryCommand, Unit>
    {
        private readonly ICallHistoryRepositories _callRepo;


        public UpdateCallHistoryCommandHandler(ICallHistoryRepositories callRepo)
        {
            _callRepo = callRepo;
        }
        public async Task<Unit> Handle(UpdateCallHistoryCommand request, CancellationToken cancellationToken)
        {
            var call = new CallHistory();
            call.Id = request.Id;
            call.CallNumber = request.CallNumber;
            call.UserId = request.UserId;
            call.CustomerId = request.CustomerId;
            call.RealTimeCall = request.RealTimeCall;
            call.StatusCall = request.StatusCall;
            call.DateCall = request.DateCall;
            call.RecordLink = request.RecordLink;
            call.Direction = request.Direction;
            call.TotalTimeCall = request.TotalTimeCall;
            // await _callRepo.UpdateCallHistorySP(call);
            _callRepo.Update(call);
            await _callRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
