﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.CallHistorys.GetCallHistoryByDay
{
    public class GetCallHistoryByDateCommand : IRequest<List<CallHistoryDTO>>,  ICommand
    {
        public DateTime DateCall { get; set; }
    }
}
