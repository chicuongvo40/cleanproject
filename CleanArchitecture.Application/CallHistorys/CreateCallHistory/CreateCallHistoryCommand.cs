﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.CallHistorys.CreateCallHistory
{
    public class CreateCallHistoryCommand : IRequest<CallHistory>, ICommand
    {
        public string CallNumber { get; set; }

        public int UserId { get; set; }

        public int CustomerId { get; set; }

        public string RealTimeCall { get; set; }

        public string StatusCall { get; set; }

        public DateTime DateCall { get; set; }

        public string RecordLink { get; set; }

        public string Direction { get; set; }

        public string TotalTimeCall { get; set; }
    }
}
