﻿using CleanArchitecture.Application.Branchs.DeleteBranch;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.DeleteSchedule
{
    public class DeleteScheduleCommandHandler : IRequestHandler<DeleteScheduleCommand, Unit>
    {
        private readonly IScheduleRepositories _scheduleRepository;

        public DeleteScheduleCommandHandler(IScheduleRepositories scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }
        public async Task<Unit> Handle(DeleteScheduleCommand request, CancellationToken cancellationToken)
        {
            var branch = await _scheduleRepository.SearchByTittle(request.Tittle, cancellationToken);

            foreach (var schedule in branch)
            {
                _scheduleRepository.Remove(schedule);
            }

            await _scheduleRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

