﻿using AutoMapper;
using CleanArchitecture.Application.Customers.SearchCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.GetScheduleByTittle
{
    public class GetScheduleByTittleCommandHandler : IRequestHandler<GetScheduleByTittleCommand, ScheduleDTO>
    {
        private readonly IScheduleRepositories _cusRepository;
        private readonly IMapper _mapper;

        public GetScheduleByTittleCommandHandler(IScheduleRepositories cusRepository, IMapper mapper)
        {
            _cusRepository = cusRepository;
            _mapper = mapper;
        }

        public async Task<ScheduleDTO> Handle(GetScheduleByTittleCommand request, CancellationToken cancellationToken)
        {
            var tittle = request.Tittle;

            var schedule = await _cusRepository.SearchByTittle(tittle, cancellationToken);

            if (schedule != null)
            {
                var scheduleDTO = _mapper.Map<ScheduleDTO>(schedule.First());
                return scheduleDTO;
            }
            throw new NotFoundException($"Schedule with Tittle {tittle} not found");
        }
    }
}
