﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.GetScheduleByTittle
{
    public class GetScheduleByTittleCommand : IRequest<ScheduleDTO>, ICommand
    {
        public string Tittle { get; set; }
    }
}
