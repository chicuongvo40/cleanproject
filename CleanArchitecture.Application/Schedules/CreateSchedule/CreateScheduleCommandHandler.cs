﻿using CleanArchitecture.Application.Branchs.CreateBranch;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Schedules.CreateSchedule
{
    public class CreateScheduleCommandHandler : IRequestHandler<CreateScheduleCommand, Schedule>
    {
        private readonly IScheduleRepositories _scheduleRepository;
        public CreateScheduleCommandHandler(IScheduleRepositories scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }
        public async Task<Schedule> Handle(CreateScheduleCommand request, CancellationToken cancellationToken)
        {
            var _schedule = new Schedule(
              request.CustomerId,
               request.StaffId,
               request.Tittle,
               request.Status,
               request.Note,
               request.CreatedDate,
               request.MeetTime,
               request.LastEditedTime
                );
            //await _braRepository.CreateBranchSP(_branch);
            _scheduleRepository.Add(_schedule);
            await _scheduleRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _schedule;
        }
    }
}
