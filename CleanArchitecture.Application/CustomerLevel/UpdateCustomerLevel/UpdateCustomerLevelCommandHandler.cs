﻿using CleanArchitecture.Application.Sources.UpdateSource;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.UpdateCustomerLevel
{
    public class UpdateCustomerLevelCommandHandler : IRequestHandler<UpdateCustomerLevelCommand, Unit>
    {
        private readonly ICustomerLevelRepositories _sourceRepository;
        public UpdateCustomerLevelCommandHandler(ICustomerLevelRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Unit> Handle(UpdateCustomerLevelCommand request, CancellationToken cancellationToken)
        {
            var customerlevel = new Domain.Entities.CustomerLevel();
            customerlevel.Id = request.Id;
            customerlevel.LevelName = request.LevelName;
            _sourceRepository.Update(customerlevel);
            await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}