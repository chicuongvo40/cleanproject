﻿using CleanArchitecture.Application.Sources.DeleteSource;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.DeleteCustomerLevel
{
    public class DeleteCustomerLevelCommandHandler : IRequestHandler<DeleteCustomerLevelCommand, Unit>
    {
        private readonly ICustomerLevelRepositories _sourceRepository;

        public DeleteCustomerLevelCommandHandler(ICustomerLevelRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Unit> Handle(DeleteCustomerLevelCommand request, CancellationToken cancellationToken)
        {
            var type = await _sourceRepository.FindByIdAsync(request.Id, cancellationToken);

            if (type != null)
            {
                _sourceRepository.Remove(type);
                await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
