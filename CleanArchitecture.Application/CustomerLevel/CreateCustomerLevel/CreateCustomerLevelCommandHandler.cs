﻿using CleanArchitecture.Application.Sources.CreateSource;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.CreateCustomerLevel
{
    public class CreateCustomerLevelCommandHandler : IRequestHandler<CreateCustomerLevelCommand, Domain.Entities.CustomerLevel>
    {
        private readonly ICustomerLevelRepositories _sourceRepository;
        public CreateCustomerLevelCommandHandler(ICustomerLevelRepositories sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }
        public async Task<Domain.Entities.CustomerLevel> Handle(CreateCustomerLevelCommand request, CancellationToken cancellationToken)
        {
            var _source = new Domain.Entities.CustomerLevel(
               request.LevelName
                );
            _sourceRepository.Add(_source);
            await _sourceRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return _source;

        }
    }
}