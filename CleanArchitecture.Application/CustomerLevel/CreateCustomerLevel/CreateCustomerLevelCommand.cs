﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.CustomerLevel.CreateCustomerLevel
{
    public class CreateCustomerLevelCommand : IRequest<Domain.Entities.CustomerLevel>, ICommand
    {
        public string LevelName { get; set; }

    }
}
