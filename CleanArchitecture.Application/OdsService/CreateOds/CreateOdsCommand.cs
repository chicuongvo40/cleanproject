﻿using CleanArchitecture.Domain.Entities;
using MediatR;

using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.OdsService.CreateOds
{
    public class CreateOdsCommand : IRequest<Od>, ICommand
    {
        public string OdsBranch { get; set; }

        public string OdsPassword { get; set; }
    }
}
