﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.CreateOds
{
    public class CreateOdsCommandHandler : IRequestHandler<CreateOdsCommand, Od>
    {
        private readonly IOdsRepositories _odsRepository;

        public CreateOdsCommandHandler(IOdsRepositories odsRepository)
        {
            _odsRepository = odsRepository;
        }
        public async Task<Od> Handle(CreateOdsCommand request, CancellationToken cancellationToken)
        {
           var ods = new Od(
               request.OdsBranch,
               request.OdsPassword
               );
            _odsRepository.Add(ods);
            await _odsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return ods;
        }
    }
}
