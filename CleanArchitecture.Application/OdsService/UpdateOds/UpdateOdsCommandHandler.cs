﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.UpdateOds
{
    public class UpdateOdsCommandHandler : IRequestHandler<UpdateOdsCommand, Unit>
    {
        private readonly IOdsRepositories _odsRepository;
        public UpdateOdsCommandHandler(IOdsRepositories odsRepository)
        {
            _odsRepository = odsRepository;
        }
        public async Task<Unit> Handle(UpdateOdsCommand request, CancellationToken cancellationToken)
        {
            var ods = new Od();
            ods.Id = request.Id;
            ods.OdsBranch = request.OdsBranch;
            ods.OdsPassword = request.OdsPassword;
            _odsRepository.Update(ods);
            await _odsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
