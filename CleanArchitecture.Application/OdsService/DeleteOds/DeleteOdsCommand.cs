﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.OdsService.DeleteOds
{
    public class DeleteOdsCommand : IRequest<Unit>, ICommand
    {
        public DeleteOdsCommand(int odsId)
        {
            Id = odsId;
        }
        public int Id { get; set; }
    }
}
