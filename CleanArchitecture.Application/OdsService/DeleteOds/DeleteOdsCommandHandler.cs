﻿using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.OdsService.DeleteOds
{
    public class DeleteOdsCommandHandler : IRequestHandler<DeleteOdsCommand, Unit>
    {
        private readonly IOdsRepositories _odsRepository;
        public DeleteOdsCommandHandler(IOdsRepositories odsRepository)
        {
            _odsRepository = odsRepository;
        }
        public async Task<Unit> Handle(DeleteOdsCommand request, CancellationToken cancellationToken)
        {
            var ods = await _odsRepository.FindByIdAsync(request.Id, cancellationToken);

            if (ods != null)
            {
                _odsRepository.Remove(ods);
                await _odsRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            }
            return Unit.Value;
        }
    }
}
