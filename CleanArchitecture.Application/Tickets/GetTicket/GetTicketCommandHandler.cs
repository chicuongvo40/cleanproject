﻿using AutoMapper;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicket
{
    public class GetTicketCommandHandler : IRequestHandler<GetTicketCommand, PagedResults<TicketDTO>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        public GetTicketCommandHandler(ITicketRepositories ticketRepo, IMapper mapper)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketDTO>> Handle(GetTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.GetTicketSP();
            var ticketDTO = _mapper.Map<List<TicketDTO>>(ticket);
            var op = PageHelper<TicketDTO>.Paging(ticketDTO, request.page, request.size);
            return op;
        }
    }
}
