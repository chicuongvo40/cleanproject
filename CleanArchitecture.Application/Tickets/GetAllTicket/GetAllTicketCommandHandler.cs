﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicket;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetAllTicket
{
    public class GetAllTicketCommandHandler : IRequestHandler<GetAllTicketCommand, PagedResults<Ticket>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        public GetAllTicketCommandHandler(ITicketRepositories ticketRepo, IMapper mapper)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
        }
        public async Task<PagedResults<Ticket>> Handle(GetAllTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.GetTicketSP();
            //var ticketDTO = _mapper.Map<List<TicketDTO>>(ticket);
            var op = PageHelper<Ticket>.Paging(ticket, request.page, request.size);
            return op;
        }
    }
}

