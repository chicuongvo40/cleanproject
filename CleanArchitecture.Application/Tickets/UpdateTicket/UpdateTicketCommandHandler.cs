﻿using CleanArchitecture.Application.Customers.UpdateCustomer;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.UpdateTicket
{
    public class UpdateTicketCommandHandler : IRequestHandler<UpdateTicketCommand, Unit>
    {
        private readonly ITicketRepositories _ticketRepo;
        public UpdateTicketCommandHandler(ITicketRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Unit> Handle(UpdateTicketCommand request, CancellationToken cancellationToken)
        {
            var ticket = new Ticket();
            ticket.Id = request.Id;
            ticket.CallId = request.CallId;
            ticket.Note = request.Note;
            ticket.CustomerId = request.CustomerId;
            ticket.TicketTypeId = request.TicketTypeId;
            ticket.LevelId = request.LevelId;
            ticket.TicketStatusId = request.TicketStatusId;
            ticket.CreatedBy = request.CreatedBy;
            //await _ticketRepo.UpdateTicketSP(ticket);
            _ticketRepo.Update(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}