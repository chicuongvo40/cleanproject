﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using MediatR;

namespace CleanArchitecture.Application.Tickets.UpdateTicket
{
    public class UpdateTicketCommand :IRequest<Unit>, ICommand
    {
        public int Id { get; set; }

        public int CallId { get; set; }

        public string Note { get; set; }

        public int CustomerId { get; set; }

        public int TicketTypeId { get; set; }

        public int LevelId { get; set; }

        public int TicketStatusId { get; set; }

        public DateTime CreatedBy { get; set; }
    }
}
