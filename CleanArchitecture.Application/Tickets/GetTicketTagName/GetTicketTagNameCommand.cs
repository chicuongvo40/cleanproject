﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketTagName
{
    public class GetTicketTagNameCommand : IRequest<PagedResults<TicketTagDTO>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}

