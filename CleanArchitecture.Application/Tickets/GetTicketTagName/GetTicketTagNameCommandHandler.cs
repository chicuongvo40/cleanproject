﻿using AutoMapper;
using CleanArchitecture.Application.TicketTypes.GetTicketType;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using FFPT_Project.Service.Helpers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketTagName
{
    public class GetTicketTagNameCommandHandler : IRequestHandler<GetTicketTagNameCommand, PagedResults<TicketTagDTO>>
    {
        private readonly ITicketTagRepositories _tagRepository;
        private readonly IMapper _mapper;

        public GetTicketTagNameCommandHandler(ITicketTagRepositories typeRepository, IMapper mapper)
        {
            _tagRepository = typeRepository;
            _mapper = mapper;
        }
        public async Task<PagedResults<TicketTagDTO>> Handle(GetTicketTagNameCommand request, CancellationToken cancellationToken)
        {
            var type = await _tagRepository.FindAllAsync();
            var typeDTOs = _mapper.Map<List<TicketTagDTO>>(type);
            var op = PageHelper<TicketTagDTO>.Paging(typeDTOs, request.page, request.size);
            return op;
        }
    }
}
