﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicketById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketByStatus
{
    public class GetTicketByStatusCommandHandler : IRequestHandler<GetTicketByStatusCommand, TicketDTO>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        public GetTicketByStatusCommandHandler(ITicketRepositories ticketRepo, IMapper mapper)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
        }

        public async Task<TicketDTO> Handle(GetTicketByStatusCommand request, CancellationToken cancellationToken)
        {
            var statusId = 1; 
            var ticket = await _ticketRepo.GetTicketByIdSP(statusId, cancellationToken);

            if (ticket != null)
            {
                var ticketDTO = _mapper.Map<TicketDTO>(ticket.First());
                return ticketDTO;
            }
            throw new NotFoundException($"Ticket with ID {statusId} not found");
        }
    }
}
