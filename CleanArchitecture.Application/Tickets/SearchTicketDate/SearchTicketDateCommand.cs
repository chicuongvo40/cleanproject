﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.SearchTicketDate
{
    public class SearchTicketDateCommand : IRequest<TicketDTO>, ICommand
    {
        public DateTime CreatedBy { get; set; }
    }
}
