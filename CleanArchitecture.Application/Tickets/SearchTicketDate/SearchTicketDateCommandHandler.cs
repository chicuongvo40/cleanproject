﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicketById;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.SearchTicketDate
{
    public class SearchTicketDateCommandHandler : IRequestHandler<SearchTicketDateCommand, TicketDTO>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        public SearchTicketDateCommandHandler(ITicketRepositories ticketRepo, IMapper mapper)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
        }

        public async Task<TicketDTO> Handle(SearchTicketDateCommand request, CancellationToken cancellationToken)
        {
            var date = request.CreatedBy;
            var ticket = await _ticketRepo.GetTicketByDateSP(date, cancellationToken);

            if (ticket != null)
            {
                var ticketDTO = _mapper.Map<TicketDTO>(ticket.First());
                return ticketDTO;
            }
            throw new NotFoundException($"Ticket with date {date} not found");
        }
    }
}
