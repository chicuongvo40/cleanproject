﻿using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.CreateTicket
{
    public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, Ticket>
    {
        private readonly ITicketRepositories _ticketRepo;
        public CreateTicketCommandHandler(ITicketRepositories ticketRepo)
        {
            _ticketRepo = ticketRepo;
        }
        public async Task<Ticket> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
        {
            List<Log> listlog = new List<Log>();
            var _log = new Log(
               );
            _log.Note = "Ticket đã được tạo";
            _log.CreatedDate = DateTime.Now;
            _log.Reason = "";
            _log.TimeStart = DateTime.Now;
            _log.UserId = 1;
            listlog.Add( _log );
            var ticket = new Ticket(
              request.CallId,
              request.Note,
              request.CustomerId,
              request.TicketTypeId,
              request.LevelId,
              request.TicketStatusId,
              request.CreatedBy,
              listlog
               );
            //await _ticketRepo.CreateTicketSP(ticket);
            _ticketRepo.Add(ticket);
            await _ticketRepo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return ticket;
        }
    }
}
