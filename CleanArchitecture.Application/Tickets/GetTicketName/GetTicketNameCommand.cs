﻿using CleanArchitecture.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;

namespace CleanArchitecture.Application.Tickets.GetTicket
{
    public class GetTicketNameCommand : IRequest<PagedResults<TicketName>>, ICommand
    {
        public int page { get; set; }

        public int size { get; set; }
    }
}
