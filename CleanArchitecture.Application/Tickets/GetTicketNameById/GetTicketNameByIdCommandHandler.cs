﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicket;
using CleanArchitecture.Application.Tickets.GetTicketById;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketNameById
{
    public class GetTicketNameByIdCommandHandler : IRequestHandler<GetTicketNameByIdCommand, List<TicketName>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly ICallHistoryRepositories _callHistory;
        private readonly ICustomerRepositories _customer;
        private readonly ILevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        public GetTicketNameByIdCommandHandler(ITicketRepositories ticketRepo, IMapper mapper, ICallHistoryRepositories callHistory, ICustomerRepositories customer, ILevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _callHistory = callHistory;
            _customer = customer;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
        }
        public async Task<List<TicketName>> Handle(GetTicketNameByIdCommand request, CancellationToken cancellationToken)
        {
            var ticket = await _ticketRepo.GetTicketByIdSP(request.TicketId);
            if (ticket == null)
            {
                return null;
            }
            List<TicketName> list = new List<TicketName>();
            foreach (var item in ticket)
            {
                var ops = new TicketName();
                ops.Id = item.Id;
                ops.CallId = _callHistory.FindByIdAsync(item.CallId).Result.CallNumber;
                ops.Note = item.Note;
                ops.CustomerId = _customer.FindByIdAsync(item.CustomerId).Result.Name;
                ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                ops.CreatedBy = item.CreatedBy;
                list.Add(ops);
            }

            return list;
        }
    }
}