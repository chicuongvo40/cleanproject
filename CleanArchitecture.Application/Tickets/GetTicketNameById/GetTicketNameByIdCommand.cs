﻿using CleanArchitecture.Application.Common.Interfaces;
using CleanArchitecture.Domain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketNameById
{
    public class GetTicketNameByIdCommand : IRequest<List<TicketName>>, ICommand
    {
        public int TicketId { get; set; }
    }
}
