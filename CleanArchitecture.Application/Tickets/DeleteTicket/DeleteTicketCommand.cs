﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanArchitecture.Application.Common.Interfaces;

namespace CleanArchitecture.Application.Tickets.DeleteTicket
{
    public class DeleteTicketCommand : IRequest<Unit>, ICommand
    {

        public DeleteTicketCommand(int _ticketId)
        {
            Id = _ticketId;
        }

        public int Id { get; set; }
    }
}
