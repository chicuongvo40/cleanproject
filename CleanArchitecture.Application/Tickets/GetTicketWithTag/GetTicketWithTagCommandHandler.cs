﻿using AutoMapper;
using CleanArchitecture.Application.Tickets.GetTicketNameByCustomerId;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Application.Tickets.GetTicketWithTag
{
    public class GetTicketWithTagCommandHandler : IRequestHandler<GetTicketWithTagCommand, List<TicketWithTagDTO>>
    {
        private readonly ITicketRepositories _ticketRepo;
        private readonly IMapper _mapper;
        private readonly ICallHistoryRepositories _callHistory;
        private readonly ILevelRepositories _level;
        private readonly ITicketTypeRepositories _ticketType;
        private readonly ITicketStatusRepositories _ticketStatus;
        private readonly ITicketTagConnectRepositories _ticketTagConnectRepositories;
        private readonly ITicketTagRepositories _ticketTag;

        public GetTicketWithTagCommandHandler(ITicketRepositories ticketRepo, IMapper mapper, ICallHistoryRepositories callHistory, ILevelRepositories level, ITicketTypeRepositories ticketType, ITicketStatusRepositories ticketStatus, ITicketTagConnectRepositories ticketTagConnectRepositories, ITicketTagRepositories ticketTag)
        {
            _ticketRepo = ticketRepo;
            _mapper = mapper;
            _callHistory = callHistory;
            _level = level;
            _ticketType = ticketType;
            _ticketStatus = ticketStatus;
            _ticketTagConnectRepositories = ticketTagConnectRepositories;
            _ticketTag = ticketTag;
        }
        public async Task<List<TicketWithTagDTO>> Handle(GetTicketWithTagCommand request, CancellationToken cancellationToken)
        {

            var ticket = await _ticketRepo.FindByStatusIdAsync(request.StatusId);
            if (ticket == null)
            {
                return null;
            }
            List<TicketWithTagDTO> list = new List<TicketWithTagDTO>();
            foreach (var item in ticket)
            {
                var ops = new TicketWithTagDTO();
                ops.Id = item.Id;
                ops.CallId = _callHistory.FindByIdAsync(item.CallId).Result.CallNumber;
                ops.Note = item.Note;
                ops.CustomerId = item.CustomerId.ToString();
                ops.LevelId = _level.FindByIdAsync(item.LevelId).Result.LevelName;
                ops.TicketTypeId = _ticketType.FindByIdAsync(item.TicketTypeId).Result.TicketTypeName;
                ops.TicketStatusId = _ticketStatus.FindByIdAsync(item.TicketStatusId).Result.StatusName;
                ops.CreatedBy = item.CreatedBy;
                list.Add(ops);
            }
            foreach (var item in list)
            {
                var op = await _ticketTagConnectRepositories.FindByTicketId(item.Id);
                List<string> listag = new List<string>();
                foreach (var item2 in op) 
                {
                    var ad = await _ticketTag.FindByIdAsync(item2.TicketTagId);
                    listag.Add(ad.TagName);
                }
                item.Tags = listag;
            }
            return list;
        }
    }
}
