﻿using CleanArchitecture.Domain.Common.Interfaces;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IUnitOfWork
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }

       // public DbSet<User> Users { get; set; }

        public virtual DbSet<Branch> Branches { get; set; }

        public virtual DbSet<CallHistory> CallHistories { get; set; }

        public virtual DbSet<Contract> Contracts { get; set; }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<CustomerLevel> CustomerLevels { get; set; }

        public virtual DbSet<Level> Levels { get; set; }

        public virtual DbSet<Log> Logs { get; set; }

        public virtual DbSet<Od> Ods { get; set; }

        public virtual DbSet<Role> Roles { get; set; }

        public virtual DbSet<Schedule> Schedules { get; set; }

        public virtual DbSet<Source> Sources { get; set; }

        public virtual DbSet<Survey> Surveys { get; set; }

        public virtual DbSet<Ticket> Tickets { get; set; }

        public virtual DbSet<TicketAsign> TicketAsigns { get; set; }

        public virtual DbSet<TicketImage> TicketImages { get; set; }

        public virtual DbSet<TicketStatus> TicketStatuses { get; set; }

        public virtual DbSet<TicketTag> TicketTags { get; set; }

        public virtual DbSet<TicketTagConnect> TicketTagConnects { get; set; }

        public virtual DbSet<TicketType> TicketTypes { get; set; }

        public virtual DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ConfigureModel(modelBuilder);
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
        }

        
        private void ConfigureModel(ModelBuilder modelBuilder)
        {
            // Seed data
            // https://rehansaeed.com/migrating-to-entity-framework-core-seed-data/
            /* Eg.
            
            modelBuilder.Entity<Car>().HasData(
            new Car() { CarId = 1, Make = "Ferrari", Model = "F40" },
            new Car() { CarId = 2, Make = "Ferrari", Model = "F50" },
            new Car() { CarId = 3, Make = "Lamborghini", Model = "Countach" });
            */
        }
    }
}
