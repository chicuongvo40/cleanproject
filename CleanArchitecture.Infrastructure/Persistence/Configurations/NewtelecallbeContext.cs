﻿using System;
using System.Collections.Generic;
using CleanArchitecture.Domain.Common.Interfaces;
using CleanArchitecture.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CleanArchitecture.Infrastructure.Persistence.Configurations;

public partial class NewtelecallbeContext : DbContext, IUnitOfWork
{
    public NewtelecallbeContext()
    {
    }

    public NewtelecallbeContext(DbContextOptions<NewtelecallbeContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Branch> Branches { get; set; }

    public virtual DbSet<CallHistory> CallHistories { get; set; }

    public virtual DbSet<Contract> Contracts { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<CustomerLevel> CustomerLevels { get; set; }

    public virtual DbSet<Level> Levels { get; set; }

    public virtual DbSet<Log> Logs { get; set; }

    public virtual DbSet<Od> Ods { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<Schedule> Schedules { get; set; }

    public virtual DbSet<Source> Sources { get; set; }

    public virtual DbSet<Survey> Surveys { get; set; }

    public virtual DbSet<Ticket> Tickets { get; set; }

    public virtual DbSet<TicketAsign> TicketAsigns { get; set; }

    public virtual DbSet<TicketImage> TicketImages { get; set; }

    public virtual DbSet<TicketStatus> TicketStatuses { get; set; }

    public virtual DbSet<TicketTag> TicketTags { get; set; }

    public virtual DbSet<TicketTagConnect> TicketTagConnects { get; set; }

    public virtual DbSet<TicketType> TicketTypes { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
    //=> optionsBuilder.UseSqlServer("server =DESKTOP-ETC4R5E\\CHICUONG; database = newtelecallbe;uid=sa;pwd=123456;TrustServerCertificate=True;");
    => optionsBuilder.UseSqlServer("Server=tcp:171.248.95.98,1433;Initial Catalog=cuong;Persist Security Info=False;User ID=cuong;Password=Gpaw23h15O3R;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30");
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Branch>(entity =>
        {
            entity.ToTable("Branch");

            entity.Property(e => e.Address).HasMaxLength(255);
            entity.Property(e => e.BranchName).HasMaxLength(255);
            entity.Property(e => e.OdsId).HasColumnName("Ods_Id");

            entity.HasOne(d => d.Ods).WithMany(p => p.Branches)
                .HasForeignKey(d => d.OdsId)
                .HasConstraintName("FK_Branch_ODS");
        });

        modelBuilder.Entity<CallHistory>(entity =>
        {
            entity.ToTable("CallHistory");

            entity.Property(e => e.CallNumber).HasMaxLength(255);
            entity.Property(e => e.DateCall).HasColumnType("datetime");
            entity.Property(e => e.Direction).HasMaxLength(255);
            entity.Property(e => e.RealTimeCall).HasMaxLength(255);
            entity.Property(e => e.RecordLink).HasMaxLength(255);
            entity.Property(e => e.StatusCall).HasMaxLength(255);
            entity.Property(e => e.TotalTimeCall).HasMaxLength(255);

            entity.HasOne(d => d.Customer).WithMany(p => p.CallHistories)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_CallHistory_Customer");

            entity.HasOne(d => d.User).WithMany(p => p.CallHistories)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_CallHistory_User");
        });

        modelBuilder.Entity<Contract>(entity =>
        {
            entity.ToTable("Contract");

            entity.Property(e => e.ContractType).HasMaxLength(255);
            entity.Property(e => e.LastEditedTime).HasColumnType("datetime");
            entity.Property(e => e.TermsAndConditions).HasMaxLength(255);
            entity.Property(e => e.TimeEnd).HasColumnType("datetime");
            entity.Property(e => e.TimeStart).HasColumnType("datetime");

            entity.HasOne(d => d.Customer).WithMany(p => p.Contracts)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_Contract_Customer");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.ToTable("Customer");

            entity.Property(e => e.Address).HasMaxLength(255);
            entity.Property(e => e.DateCreated).HasColumnType("datetime");
            entity.Property(e => e.DayOfBirth).HasColumnType("datetime");
            entity.Property(e => e.FirstName).HasMaxLength(255);
            entity.Property(e => e.Gender)
                .HasMaxLength(255)
                .HasColumnName("Gender ");
            entity.Property(e => e.LastEditedTime).HasColumnType("datetime");
            entity.Property(e => e.LastName).HasMaxLength(255);
            entity.Property(e => e.Name).HasMaxLength(255);
            entity.Property(e => e.PhoneNumber).HasMaxLength(255);
            entity.Property(e => e.Status).HasMaxLength(255);

            entity.HasOne(d => d.Branch).WithMany(p => p.Customers)
                .HasForeignKey(d => d.BranchId)
                .HasConstraintName("FK_Customer_Branch");

            entity.HasOne(d => d.CustomerLevel).WithMany(p => p.Customers)
                .HasForeignKey(d => d.CustomerLevelId)
                .HasConstraintName("FK_Customer_CustomerLevel");

            entity.HasOne(d => d.Source).WithMany(p => p.Customers)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("FK_Customer_Source");
        });

        modelBuilder.Entity<CustomerLevel>(entity =>
        {
            entity.ToTable("CustomerLevel");

            entity.Property(e => e.LevelName).HasMaxLength(255);
        });

        modelBuilder.Entity<Level>(entity =>
        {
            entity.ToTable("Level");

            entity.Property(e => e.LevelName)
                .HasMaxLength(255)
                .HasColumnName("Level_Name");
        });

        modelBuilder.Entity<Log>(entity =>
        {
            entity.ToTable("Log");

            entity.Property(e => e.AssignedTo).HasMaxLength(255);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.IsAssigned).HasMaxLength(255);
            entity.Property(e => e.Note).HasMaxLength(255);
            entity.Property(e => e.Reason).HasMaxLength(255);
            entity.Property(e => e.TimeEnd).HasColumnType("datetime");
            entity.Property(e => e.TimeStart).HasColumnType("datetime");

            entity.HasOne(d => d.Ticket).WithMany(p => p.Logs)
                .HasForeignKey(d => d.TicketId)
                .HasConstraintName("FK_Log_Ticket");

            entity.HasOne(d => d.User).WithMany(p => p.Logs)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_Log_User");
        });

        modelBuilder.Entity<Od>(entity =>
        {
            entity.ToTable("ODS");

            entity.Property(e => e.OdsBranch).HasMaxLength(255);
            entity.Property(e => e.OdsPassword).HasMaxLength(255);
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.ToTable("Role");

            entity.Property(e => e.Name).HasMaxLength(255);
        });

        modelBuilder.Entity<Schedule>(entity =>
        {
            entity.ToTable("Schedule");

            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.LastEditedTime).HasColumnType("datetime");
            entity.Property(e => e.MeetTime).HasColumnType("datetime");
            entity.Property(e => e.Note).HasMaxLength(255);
            entity.Property(e => e.Status).HasMaxLength(255);
            entity.Property(e => e.Tittle).HasMaxLength(255);

            entity.HasOne(d => d.Customer).WithMany(p => p.Schedules)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_Schedule_Customer");

            entity.HasOne(d => d.Staff).WithMany(p => p.Schedules)
                .HasForeignKey(d => d.StaffId)
                .HasConstraintName("FK_Schedule_User");
        });

        modelBuilder.Entity<Source>(entity =>
        {
            entity.ToTable("Source");

            entity.Property(e => e.SourceName).HasMaxLength(255);
        });

        modelBuilder.Entity<Survey>(entity =>
        {
            entity.ToTable("Survey");

            entity.Property(e => e.Comment).HasMaxLength(255);
            entity.Property(e => e.SatisfactionRating).HasMaxLength(255);
            entity.Property(e => e.SurveyDate).HasColumnType("datetime");

            entity.HasOne(d => d.Customer).WithMany(p => p.Surveys)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_Survey_Customer");
        });

        modelBuilder.Entity<Ticket>(entity =>
        {
            entity.ToTable("Ticket");

            entity.Property(e => e.CallId).HasColumnName("Call_Id");
            entity.Property(e => e.CreatedBy).HasColumnType("datetime");
            entity.Property(e => e.Note).HasMaxLength(255);

            entity.HasOne(d => d.Level).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.LevelId)
                .HasConstraintName("FK_Ticket_Level");

            entity.HasOne(d => d.TicketStatus).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.TicketStatusId)
                .HasConstraintName("FK_Ticket_TicketStatus");

            entity.HasOne(d => d.TicketType).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.TicketTypeId)
                .HasConstraintName("FK_Ticket_TicketType");
        });

        modelBuilder.Entity<TicketAsign>(entity =>
        {
            entity.ToTable("TicketAsign");

            entity.Property(e => e.Status).HasMaxLength(255);
            entity.Property(e => e.TimeEnd).HasColumnType("datetime");
            entity.Property(e => e.TimeStart).HasColumnType("datetime");

            entity.HasOne(d => d.Ticket).WithMany(p => p.TicketAsigns)
                .HasForeignKey(d => d.TicketId)
                .HasConstraintName("FK_TicketAsign_Ticket");

            entity.HasOne(d => d.User).WithMany(p => p.TicketAsigns)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_TicketAsign_User");
        });

        modelBuilder.Entity<TicketImage>(entity =>
        {
            entity.ToTable("TicketImage");

            entity.Property(e => e.ImageUrl).HasMaxLength(255);

            entity.HasOne(d => d.Log).WithMany(p => p.TicketImages)
                .HasForeignKey(d => d.LogId)
                .HasConstraintName("FK_TicketImage_Log");
        });

        modelBuilder.Entity<TicketStatus>(entity =>
        {
            entity.ToTable("TicketStatus");

            entity.Property(e => e.StatusName)
                .HasMaxLength(255)
                .HasColumnName("Status_Name");
        });

        modelBuilder.Entity<TicketTag>(entity =>
        {
            entity.ToTable("TicketTag");

            entity.Property(e => e.TagName).HasMaxLength(255);
        });

        modelBuilder.Entity<TicketTagConnect>(entity =>
        {
            entity.ToTable("TicketTagConnect");

            entity.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("ID");

            entity.HasOne(d => d.Ticket).WithMany(p => p.TicketTagConnects)
                .HasForeignKey(d => d.TicketId)
                .HasConstraintName("FK_TicketTagConnect_Ticket");

            entity.HasOne(d => d.TicketTag).WithMany(p => p.TicketTagConnects)
                .HasForeignKey(d => d.TicketTagId)
                .HasConstraintName("FK_TicketTagConnect_TicketTag");
        });

        modelBuilder.Entity<TicketType>(entity =>
        {
            entity.ToTable("TicketType");

            entity.Property(e => e.KpiDuration).HasMaxLength(255);
            entity.Property(e => e.TicketTypeName).HasMaxLength(255);
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("User");

            entity.Property(e => e.Address).HasMaxLength(255);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.DayOfBirth).HasColumnType("datetime");
            entity.Property(e => e.FirstName).HasMaxLength(255);
            entity.Property(e => e.Gender)
                .HasMaxLength(255)
                .HasColumnName("Gender ");
            entity.Property(e => e.LastName).HasMaxLength(255);
            entity.Property(e => e.Password).HasMaxLength(255);
            entity.Property(e => e.Status).HasMaxLength(255);
            entity.Property(e => e.UserName).HasMaxLength(255);

            entity.HasOne(d => d.Branch).WithMany(p => p.Users)
                .HasForeignKey(d => d.BranchId)
                .HasConstraintName("FK_User_Branch");

            entity.HasOne(d => d.Role).WithMany(p => p.Users)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK_User_Role");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
