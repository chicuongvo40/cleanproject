﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class ContractRepositories : RepositoryBase<Contract, Contract, NewtelecallbeContext>, IContractRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public ContractRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Contract?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Contract>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<Contract?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<Contract>> GetContractSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Contract>().FromSqlInterpolated($"Exec Contract_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }

        public async Task<List<Contract>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Contracts
                .Where(u => u.CustomerId == customerId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Contract>> GetContractByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Contract>().FromSqlInterpolated($"Exec Contract_SP @Operation=\"SEARCH_ID\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
