﻿using AutoMapper;
using CleanArchitecture.Api.Models;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class CallHistoryRepositories : RepositoryBase<CallHistory, CallHistory, NewtelecallbeContext>, ICallHistoryRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public CallHistoryRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<CallHistory?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<CallHistory>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<CallHistory>> SEARCH_DAY(DateTime date, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<CallHistory>().FromSqlInterpolated($"Exec CallHistory_SP @Operation=\"SEARCH_DAY\",@DateCall={date}").ToListAsync(cancellationToken);
        }
        public async Task<List<CallHistory>> SearchByDirection(string direction, CancellationToken cancellationToken)
        {
            return await FindAllAsync(x => x.Direction == direction, cancellationToken);
        }
        public async Task<List<CallHistory>> GetCallHistorySP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<CallHistory>().FromSqlInterpolated($"Exec CallHistory_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<CallHistory>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.CallHistories
                .Where(u => u.CustomerId == customerId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<CallHistory>> GetCallHistoryByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<CallHistory>().FromSqlInterpolated($"Exec CallHistory_SP @Operation=\"SEARCH\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
