﻿using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class ReportRepository : IReportRepository
    {
        private readonly NewtelecallbeContext context;
        public ReportRepository(NewtelecallbeContext context)
        {//Viet di Vo.Coi json no ra gi
            this.context = context;
        }

        //Báo cáo tổng ticket
        public async Task<object> GetTicketReport()
        {
            var tickets = await context.Tickets.ToListAsync(); // Lấy danh sách tất cả các vé từ cơ sở dữ liệu
            var total = tickets.Count; // Số lượng tổng số vé
            object result = new { total }; // Tạo object kết quả
            return result; // Trả về kết quả
        }



        public async Task<object> GetTotalCallDetail(DateTime startDate, DateTime endDate)
        {
            var query = context.CallHistories.Where(x => x.DateCall >= startDate && x.DateCall <= endDate);
            var totalCall = query.Count();
            // Gọi Thành Công, Máy Bận, Người Gọi Tắt máy
            var totalAnswer = query.Where(x => x.StatusCall == "Bắt máy").Count();
            var totalNoAnswer = query.Where(x => x.StatusCall == "Máy bận").Count();
            var totalReject = query.Where(x => x.StatusCall == "Người gọi tắt máy").Count();

            var calls = await query.GroupBy(s => new { s.StatusCall, s.DateCall.Value.Day })
                                   .Select(x => new { DayOfMonth = x.Key, NumberOfCall = x.Count() })
                                   .ToListAsync();

            List<object> listCall = new List<object>();
            var answer = 0; var noAnswer = 0; var reject = 0;

            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                foreach (var call in calls)
                {
                    if (dt.Day == call.DayOfMonth.Day)
                    {
                        if (call.DayOfMonth.StatusCall == "Bắt máy")
                            answer = call.NumberOfCall;

                        if (call.DayOfMonth.StatusCall == "Máy bận")
                            noAnswer = call.NumberOfCall;

                        if (call.DayOfMonth.StatusCall == "Người gọi tắt máy")
                            reject = call.NumberOfCall;
                    }
                }

                listCall.Add(new { date = dt.ToString("yyyy-MM-dd"), answer, noAnswer, reject });
                answer = 0; noAnswer = 0; reject = 0;
            }

            object result = new { totalCall, totalAnswer, totalNoAnswer, totalReject, listCall };
            return result;
        }

        public async Task<object> GetTotalCallDetailByMonth(int month, int year)
        {
            var query = context.CallHistories.Where(x => x.DateCall >= new DateTime(year, month, 1)
                    && x.DateCall <= new DateTime(year, month, DateTime.DaysInMonth(year, month))
                    );
            var totalCall = query.Count();
            // Gọi Thành Công, Máy Bận, Người Gọi Tắt máy
            var totalAnswer = query.Where(x => x.StatusCall == "Bắt máy").Count();
            var totalNoAnswer = query.Where(x => x.StatusCall == "Máy bận").Count();
            var totalReject = query.Where(x => x.StatusCall == "Người gọi tắt máy").Count();

            var calls = await query.GroupBy(s => new { s.StatusCall, s.DateCall.Value.Day })
                                   .Select(x => new { DayOfMonth = x.Key, NumberOfCall = x.Count() })
                                   .ToListAsync();

            List<object> listCall = new List<object>();
            var answer = 0; var noAnswer = 0; var reject = 0;

            for (var dt = new DateTime(year, month, 1); dt <= new DateTime(year, month, DateTime.DaysInMonth(year, month)); dt = dt.AddDays(1))
            {
                foreach (var call in calls)
                {
                    if (dt.Day == call.DayOfMonth.Day)
                    {
                        if (call.DayOfMonth.StatusCall == "Bắt máy")
                            answer = call.NumberOfCall;

                        if (call.DayOfMonth.StatusCall == "Máy bận")
                            noAnswer = call.NumberOfCall;

                        if (call.DayOfMonth.StatusCall == "Người gọi tắt máy")
                            reject = call.NumberOfCall;
                    }
                }

                listCall.Add(new { date = dt.ToString("yyyy-MM-dd"), answer, noAnswer, reject });
                answer = 0; noAnswer = 0; reject = 0;

            }

            object result = new { totalCall, totalAnswer, totalNoAnswer, totalReject, listCall };
            return result;
        }

        public async Task<object> GetTotalTicket(DateTime startDate, DateTime endDate)
        {
            var query = context.Tickets.Where(x => x.CreatedBy >= startDate && x.CreatedBy <= endDate);
            var totalCall = query.Count();
            // Gọi Thành Công, Máy Bận, Người Gọi Tắt máy
            var a = query.Where(x => x.TicketStatusId == 1).Count();
            var b = query.Where(x => x.TicketStatusId == 2).Count();
            var c = query.Where(x => x.TicketStatusId == 3).Count();
            var d = query.Where(x => x.TicketStatusId == 4).Count();

            var calls = await query.GroupBy(s => new { s.TicketStatusId, s.CreatedBy.Value.Day })
                                   .Select(x => new { DayOfMonth = x.Key, NumberOfCall = x.Count() })
                                   .ToListAsync();

            List<object> listCall = new List<object>();
            var aa = 0; var bb = 0; var cc = 0; var dd = 0;

            for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
            {
                foreach (var call in calls)
                {
                    if (dt.Day == call.DayOfMonth.Day)
                    {
                        if (call.DayOfMonth.TicketStatusId == 1)
                            aa = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 2)
                            bb = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 3)
                            cc = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 4)
                            dd = call.NumberOfCall;
                    }
                }

                listCall.Add(new { date = dt.ToString("yyyy-MM-dd"), aa, bb, cc , dd});
                aa = 0; bb = 0; cc = 0; dd = 0;
            }

            object result = new { totalCall, a, b, c, d, listCall };
            return result;
        }

        public async Task<object> GetTotalTicketDetailByMonth(int month, int year)
        {
            var query = context.Tickets.Where(x => x.CreatedBy >= new DateTime(year, month, 1)
                    && x.CreatedBy <= new DateTime(year, month, DateTime.DaysInMonth(year, month))
                    );
            var totalCall = query.Count();
            // Gọi Thành Công, Máy Bận, Người Gọi Tắt máy
            var a = query.Where(x => x.TicketStatusId == 1).Count();
            var b = query.Where(x => x.TicketStatusId == 2).Count();
            var c = query.Where(x => x.TicketStatusId == 3).Count();
            var d = query.Where(x => x.TicketStatusId == 4).Count();

            var calls = await query.GroupBy(s => new { s.TicketStatusId, s.CreatedBy.Value.Day })
                                   .Select(x => new { DayOfMonth = x.Key, NumberOfCall = x.Count() })
                                   .ToListAsync();

            List<object> listCall = new List<object>();
            var aa = 0; var bb = 0; var cc = 0; var dd = 0;

            for (var dt = new DateTime(year, month, 1); dt <= new DateTime(year, month, DateTime.DaysInMonth(year, month)); dt = dt.AddDays(1))
            {
                foreach (var call in calls)
                {
                    if (dt.Day == call.DayOfMonth.Day)
                    {
                        if (call.DayOfMonth.TicketStatusId == 1)
                            aa = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 2)
                            bb = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 3)
                            cc = call.NumberOfCall;

                        if (call.DayOfMonth.TicketStatusId == 4)
                            dd = call.NumberOfCall;
                    }
                }

                listCall.Add(new { date = dt.ToString("yyyy-MM-dd"), aa, bb, cc, dd });
                aa = 0; bb = 0; cc = 0; dd = 0;
            }

            object result = new { totalCall, a, b, c, d, listCall };
            return result;
        }
    }
}
