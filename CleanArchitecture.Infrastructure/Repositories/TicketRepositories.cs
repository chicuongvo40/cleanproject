﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class TicketRepositories : RepositoryBase<Ticket, Ticket, NewtelecallbeContext>, ITicketRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public TicketRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Ticket?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }
        public async Task<List<Ticket>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Tickets
                .Where(u => u.CustomerId == customerId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> FindByStatusIdAsync(int statusId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Tickets
                .Where(u => u.TicketStatusId == statusId)
                .ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<Ticket?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<Ticket>> GetTicketSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Ticket>().FromSqlInterpolated($"Exec Ticket_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> GetTicketByDateSP(DateTime date, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Ticket>().FromSqlInterpolated($"Exec Ticket_SP @Operation=\"DATE\",@CreateBy={date}").ToListAsync(cancellationToken);
        }
        public async Task<List<Ticket>> GetTicketByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Ticket>().FromSqlInterpolated($"Exec Ticket_SP @Operation=\"ID\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
