﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class TicketImageRepositories : RepositoryBase<TicketImage, TicketImage, NewtelecallbeContext>, ITicketImageRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public TicketImageRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<TicketImage?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<TicketImage>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<TicketImage>> GetTicketImageSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<TicketImage>().FromSqlInterpolated($"Exec TicketImage_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<TicketImage>> GetTicketImageByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<TicketImage>().FromSqlInterpolated($"Exec TicketAsign_SP @Operation=\"SEARCH\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
