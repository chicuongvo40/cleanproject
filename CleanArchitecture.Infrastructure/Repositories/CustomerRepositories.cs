﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class CustomerRepositories : RepositoryBase<Customer, Customer, NewtelecallbeContext>, ICustomerRepositories
    {

        private readonly NewtelecallbeContext _dbContext;
        public CustomerRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Customer?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Customer>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<Customer>> FindAllCustomer(CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(cancellationToken);
        }
        public async Task<Customer?> FindByEmailAsync(string number, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.PhoneNumber == number, cancellationToken);
        }
        public async Task<List<Customer>> GetCustomerSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Customer>> SearchCustomerSP(string name, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"NAME\",@Name={name}").ToListAsync(cancellationToken);
           //return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<Customer?> FindByPhoneNumberAsync(string phoneNumber, CancellationToken cancellationToken = default)
        {
            // Implement the logic to find a customer by phone number
            return await FindAsync(c => c.PhoneNumber == phoneNumber, cancellationToken);
        }
        public async Task<List<Customer>> GetCustomerByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Customer>().FromSqlInterpolated($"Exec Customer_SP @Operation=\"SEARCH_ID\",@Id={id}").ToListAsync(cancellationToken);
        }
    }

}
