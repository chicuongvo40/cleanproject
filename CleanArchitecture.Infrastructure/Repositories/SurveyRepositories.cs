﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class SurveyRepositories : RepositoryBase<Survey, Survey, NewtelecallbeContext>, ISurveyRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public SurveyRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Survey?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Survey>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<Survey>> GetSurveySP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Survey>().FromSqlInterpolated($"Exec Survey_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<Survey>> GetSurveyByCustomerIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Survey>().FromSqlInterpolated($"Exec Survey_SP @Operation=\"CUSTOMERID\",@CustomerId={id}").ToListAsync(cancellationToken);
        }
    }
}
