﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class LevelRepositories : RepositoryBase<Level, Level, NewtelecallbeContext>, ILevelRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public LevelRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<Level?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<Level>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<Level?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<Level>> GetLevelSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Level>().FromSqlInterpolated($"Exec Level_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<int> CreateLevelSP(Level op, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Database.ExecuteSqlInterpolatedAsync($"Exec Level_SP @Operation=\"CREATE\",@LevelName = {op.LevelName}");
        }

        public async Task<int> UpdateLevelSP(Level op, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Database.ExecuteSqlInterpolatedAsync($"Exec Level_SP @Operation=\"Update\",@LevelName = {op.LevelName}, @Id={op.Id}");
        }

        public async Task<int> DeleteLevelSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Database.ExecuteSqlInterpolatedAsync($"Exec Level_SP @Operation=\"DELETE\",@Id={id}");
        }

        public async Task<List<Level>> GetLevelByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<Level>().FromSqlInterpolated($"Exec Level_SP @Operation=\"SEARCH_ID\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
