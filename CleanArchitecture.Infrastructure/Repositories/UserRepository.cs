﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using CleanArchitecture.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CleanArchitecture.Infrastructure
{
    public class UserRepository : RepositoryBase<User, User, NewtelecallbeContext>, IUserRepository
    {
        private readonly NewtelecallbeContext _dbContext;
        public UserRepository(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<User?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }
        public async Task<List<User>> FindByRoleIdAsync(int roleId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Users
                .Where(u => u.RoleId == roleId)
                .ToListAsync(cancellationToken);
        }
        public async Task<User?> Login(string username, string password, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.UserName == username && x.Password == password, cancellationToken);
        }

        public async Task<List<User>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<User?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindByIdAsync(id, cancellationToken);
        }
        public async Task<List<User>> GetUserSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<User>().FromSqlInterpolated($"Exec User_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        
        public async Task<List<User>> GetUserByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<User>().FromSqlInterpolated($"Exec User_SP @Operation=\"SEARCH\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}