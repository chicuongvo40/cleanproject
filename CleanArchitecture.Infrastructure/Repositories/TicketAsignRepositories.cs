﻿using AutoMapper;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class TicketAsignRepositories : RepositoryBase<TicketAsign, TicketAsign, NewtelecallbeContext>, ITicketAsignRepositories
    {
        private readonly NewtelecallbeContext _dbContext;
        public TicketAsignRepositories(NewtelecallbeContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
        }

        public async Task<TicketAsign?> FindByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            return await FindAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<List<TicketAsign>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default)
        {
            return await FindAllAsync(x => x.Id == ids, cancellationToken);
        }
        public async Task<List<TicketAsign>> GetTicketAsignSP(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<TicketAsign>().FromSqlInterpolated($"Exec TicketAsign_SP @Operation=\"READ\"").ToListAsync(cancellationToken);
        }
        public async Task<List<TicketAsign>> GetTicketAsignByIdSP(int id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<TicketAsign>().FromSqlInterpolated($"Exec TicketAsign_SP @Operation=\"SEARCH\",@Id={id}").ToListAsync(cancellationToken);
        }
    }
}
