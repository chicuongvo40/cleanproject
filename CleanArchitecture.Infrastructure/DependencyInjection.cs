﻿using CleanArchitecture.Domain.Common.Interfaces;
using CleanArchitecture.Domain.Repositories;
using CleanArchitecture.Infrastructure.Persistence;
using CleanArchitecture.Infrastructure.Persistence.Configurations;
using CleanArchitecture.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CleanArchitecture.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<NewtelecallbeContext>((sp, options) =>
            {
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b =>
                    {
                        b.MigrationsAssembly(typeof(NewtelecallbeContext).Assembly.FullName);
                        b.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                    });
                options.UseLazyLoadingProxies();
            });
            services.AddScoped<IUnitOfWork>(provider => provider.GetRequiredService<NewtelecallbeContext>());
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ICustomerRepositories, CustomerRepositories>();
            services.AddTransient<IBranchRepositories, BranchRepositories>();
            services.AddTransient<ICallHistoryRepositories, CallHistoryRepositories>();
            services.AddTransient<ILevelRepositories, LevelRepositories>();
            services.AddTransient<ITicketRepositories, TicketRepositories>();
            services.AddTransient<IContractRepositories, ContractRepositories>();
            services.AddTransient<ILogRepository, LogRepository>();
            services.AddTransient<IOdsRepositories, OdsRepositories>();
            services.AddTransient<ISurveyRepositories, SurveyRepositories>();
            services.AddTransient<IScheduleRepositories, ScheduleRepositories>();
            services.AddTransient<ISourceRepositories, SourceRepositories>();
            services.AddTransient<ICustomerLevelRepositories, CustomerLevelRepositories>();
            services.AddTransient<IReportRepository, ReportRepository>();
            services.AddTransient<ITicketAsignRepositories, TicketAsignRepositories>();
            services.AddTransient<ITicketStatusRepositories, TicketStatusRepositories>();
            services.AddTransient<ITicketImageRepositories, TicketImageRepositories>();
            services.AddTransient<ITicketTypeRepositories, TicketTypeRepositories>();
            services.AddTransient<ITicketTagRepositories, TicketTagRepositories>();
            services.AddTransient<ITicketTagConnectRepositories, TicketTagConnectRepositories>();

            return services;
        }
    }
}