﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.TicketTypes.CreateTicketType;
using CleanArchitecture.Application.TicketTypes.DeleteTicketType;
using CleanArchitecture.Application.TicketTypes.GetTicketType;
using CleanArchitecture.Application.TicketTypes.GetTicketTypeById;
using CleanArchitecture.Application.TicketTypes.UpdateTicketType;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    public class TicketTypeController : ControllerBase
    {
        private readonly ISender _mediator;

        public TicketTypeController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/tickettype")]
        [ProducesResponseType(typeof(PagedResults<TicketTypeDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<TicketTypeDTO>>> GetTicketType([FromQuery] GetTicketTypeCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/tickettype/{id}")]
        [ProducesResponseType(typeof(TicketTypeDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TicketTypeDTO>> GetBranchById(int id, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetTicketTypeByIdCommand { TypeId = id }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Branch with ID {id} not found");
        }
        [HttpPost("tickettype")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<TicketType>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<JsonResponse<TicketType>>> CreateTicketType(
        [FromBody] CreateTicketTypeCommand command,
        CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
            return CreatedAtAction(nameof(CreateTicketType), new JsonResponse<TicketType>(result));
        }

        [HttpDelete("api/tickettype/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteTicketType([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            await _mediator.Send(new DeleteTicketTypeCommand(typeId: id), cancellationToken);
            return Ok();
        }
        [HttpPut("api/tickettype")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateTicketType(
          [FromBody] UpdateTicketTypeCommand command,
          CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
            }
        }
    }
}
