﻿using CleanArchitecture.Application.Tickets.GetTicketTagName;
using CleanArchitecture.Domain.DTO;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CleanArchitecture.Api.Controllers
{
    public class TicketTagController : ControllerBase
    {
        private readonly ISender _mediator;

        public TicketTagController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/tickettag")]
        [ProducesResponseType(typeof(PagedResults<TicketTypeDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<TicketTypeDTO>>> GetTicketTagName([FromQuery] GetTicketTagNameCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
    }
}
