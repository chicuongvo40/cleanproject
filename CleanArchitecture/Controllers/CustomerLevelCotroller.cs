﻿using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.CustomerLevel.CreateCustomerLevel;
using CleanArchitecture.Application.CustomerLevel.DeleteCustomerLevel;
using CleanArchitecture.Application.CustomerLevel.GetCustomerLevel;
using CleanArchitecture.Application.CustomerLevel.UpdateCustomerLevel;
using CleanArchitecture.Application.Sources.CreateSource;
using CleanArchitecture.Application.Sources.DeleteSource;
using CleanArchitecture.Application.Sources.GetSource;
using CleanArchitecture.Application.Sources.UpdateSource;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    public class CustomerLevelCotroller : ControllerBase
    {
        private readonly ISender _mediator;

        public CustomerLevelCotroller(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/customerlevel")]
        [ProducesResponseType(typeof(PagedResults<CustomerLevelDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<CustomerLevelDTO>>> GetCustomerLevel([FromQuery] GetCustomerLevelCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpPost("customerlevel")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<CustomerLevel>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<JsonResponse<CustomerLevel>>> CreateSource(
       [FromBody] CreateCustomerLevelCommand command,
       CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return CreatedAtAction(nameof(CreateSource), new JsonResponse<CustomerLevel>(result));
        }
        [HttpDelete("api/customerlevel/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteCustomerLevel([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            await _mediator.Send(new DeleteCustomerLevelCommand(customerLevelId: id), cancellationToken);
            return Ok();
        }

        [HttpPut("api/customerlevel")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateCustomerLevel(
            [FromBody] UpdateCustomerLevelCommand command,
            CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
            }
        }
    }
}