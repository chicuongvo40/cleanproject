﻿using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Repositories;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace CleanArchitecture.Api.Controllers
{
    public class AuthenController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly IConfiguration _configuration;

        public AuthenController(IConfiguration configuration, IUserRepository repository)
        {   
            _repository = repository;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<AuthenDTO>> Login([FromBody] LoginModel model)
        {
            User op = await _repository.Login(model.Email, model.Password);
           
            if (op != null)
            {
                var authClaims = new List<Claim>
                {
                   new Claim("UserId", op.Id.ToString()),
                    new Claim(ClaimTypes.Email, model.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Role,op.Role.Name)
                };
                var token = GetToken(authClaims);
                var jwt = new JwtSecurityTokenHandler().WriteToken(token);
                var ops = new AuthenDTO();
                ops.Name = model.Email;
                ops.Password = model.Password;
                ops.Role = op.Role.Name;
                ops.JWT = jwt.ToString();
                return Ok(ops);
            } else
            {
                return Unauthorized();
            } 
           
        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(

                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}
