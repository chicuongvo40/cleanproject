﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Net;

namespace CleanArchitecture.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmailController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> SendEmail([FromBody] EmailRequest model)
        {
            try
            {
                string emailForSend = "cuongvcse150674@fpt.edu.vn";
                string appPasswordConfiguration = "t j n u t y q v x c r v k l j s";

                var smtpClient = new SmtpClient
                {
                    Port = 587,
                    EnableSsl = true,
                    Host = "smtp.gmail.com",
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
                };

                var message = new MailMessage()
                {
                    Subject = model.Subject,
                    Body = model.Body,
                    From = new MailAddress(emailForSend),
                };

                foreach (var toAddress in model.ToAddresses)
                {
                    message.To.Add(new MailAddress(toAddress));
                }

                await smtpClient.SendMailAsync(message);

                return Ok("Email sent successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error sending email: {ex.Message}");
            }
        }
    }
}
