﻿
using CleanArchitecture.Api.Controllers.ResponseTypes;
using CleanArchitecture.Application.LogsModel.CreateLogs;
using CleanArchitecture.Application.LogsModel.DeleteLogs;
using CleanArchitecture.Application.LogsModel.GetLogByTicketId;
using CleanArchitecture.Application.LogsModel.GetLogs;
using CleanArchitecture.Application.LogsModel.GetLogsById;
using CleanArchitecture.Application.LogsModel.SearchTimeLogs;
using CleanArchitecture.Application.LogsModel.UpdateLogs;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using DocumentFormat.OpenXml.InkML;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    //[Authorize]
    public class LogController : ControllerBase
    {
        private readonly ISender _mediator;

        public LogController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/log")]
        [ProducesResponseType(typeof(PagedResults<LogDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<LogDTO>>> GetLog([FromQuery] GetLogCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/log/{userId}")]
        [ProducesResponseType(typeof(LogDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<LogDTO>> GetLogByUserId( int userId, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetLogByUserIdCommand { UserId = userId }, cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Log with ID {userId} not found");
        }
        [HttpGet("api/log/ticketId")]
        [ProducesResponseType(typeof(List<LogDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<LogDTO>>> GetLogByTicketId(int ticketId, CancellationToken cancellationToken = default)
        {
            var command = new GetLogByTicketIdCommand { TicketId = ticketId };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return NotFound($"No users found");
        }
        [HttpGet("api/log/SearchTime")]
        [ProducesResponseType(typeof(LogDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<LogDTO>> SearchTineLogs(DateTime timeStart, DateTime timeEnd, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new SearchaTimeLogCommand { TimeStart = timeStart , TimeEnd = timeEnd }  , cancellationToken);

            if (result != null)
            {
                return Ok(result);
            }
            return NotFound($"Log with time not found");
        }
        [HttpPost("log")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<Domain.Entities.Log>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<JsonResponse<Domain.Entities.Log>>> CreateLog(
      [FromBody] CreateLogCommand command,
      CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
            return CreatedAtAction(nameof(CreateLog), new JsonResponse<Domain.Entities.Log>(result));
        }
        [HttpDelete("api/Log/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteLog([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            await _mediator.Send(new DeleteLogCommand(logid: id), cancellationToken);
            return Ok();
        }

        [HttpPut("api/Log")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateLog(
            [FromBody] UpdateLogCommand command,
            CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
            }
        }
    }
}