﻿using CleanArchitecture.Api.Controllers.ResponseTypes;

using CleanArchitecture.Application.CallHistorys.CreateCallHistory;
using CleanArchitecture.Application.CallHistorys.DeleteCallHistory;
using CleanArchitecture.Application.CallHistorys.GetCallHistory;
using CleanArchitecture.Application.CallHistorys.GetCallHistoryByCustomerId;
using CleanArchitecture.Application.CallHistorys.GetCallHistoryByDay;
using CleanArchitecture.Application.CallHistorys.UpdateCallHistory;
using CleanArchitecture.Application.Users.GetUserByRoleId;
using CleanArchitecture.Domain.Common.Exceptions;
using CleanArchitecture.Domain.DTO;
using CleanArchitecture.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace CleanArchitecture.Api.Controllers
{
    //[Authorize]
    public class CallHistoryCotroller : ControllerBase
    {
        private readonly ISender _mediator;

        public CallHistoryCotroller(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        [HttpGet("api/callHistory")]
        [ProducesResponseType(typeof(PagedResults<CallHistoryDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PagedResults<CallHistoryDTO>>> GetCallHistory([FromQuery] GetCallHistoryCommand command, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
        [HttpGet("api/callHistory/customerId")]
        [ProducesResponseType(typeof(List<CallHistoryDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<CallHistoryDTO>>> GetCallHistoryByCustomerId(int customerId, CancellationToken cancellationToken = default)
        {
            var command = new GetCallHistoryByCustomerIdCommand { CustomerId = customerId };
            var result = await _mediator.Send(command, cancellationToken);

            if (result != null && result.Count > 0)
            {
                return Ok(result);
            }

            return NotFound($"No users found with Role ID {customerId}");
        }
        [HttpGet("api/callHistoryByDay")]
        [ProducesResponseType(typeof(List<CallHistory>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<CallHistory>>> GetCallHistoryByDay(
       [FromQuery] DateTime date,
       CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(new GetCallHistoryByDateCommand { DateCall = date }, cancellationToken);
                return Ok(result);
            }
            catch (Exception ex)
            {
                // Log the exception and return a 500 Internal Server Error response
                return StatusCode(StatusCodes.Status500InternalServerError, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpGet("api/callHistoryByDirection")]
        [ProducesResponseType(typeof(List<CallHistory>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<CallHistory>>> GetCallHistoryByDirection(
    [FromQuery] string direction,
    CancellationToken cancellationToken = default)
        {
            try
            {
                // Validate the direction parameter
                if (string.IsNullOrWhiteSpace(direction) ||
                    (direction != "Inbound" && direction != "Outbound"))
                {
                    return BadRequest("Invalid direction. Direction must be 'Inbound' or 'Outbound'.");
                }

                var result = await _mediator.Send(new GetCallHistoryByDirectionQuery { Direction = direction }, cancellationToken);
                return Ok(result);
            }
            catch (Exception ex)
            {
                // Log the exception and return a 500 Internal Server Error response
                return StatusCode(StatusCodes.Status500InternalServerError, $"Internal Server Error: {ex.Message}");
            }
        }
        [HttpPost("callHistory")]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(JsonResponse<CallHistory>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<JsonResponse<CallHistory>>> CreateCallHistory(
       [FromBody] CreateCallHistoryCommand command,
       CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(command, cancellationToken);
            //return CreatedAtAction(nameof(GetOrderById), new { id = result }, new JsonResponse<Guid>(result));
            return CreatedAtAction(nameof(CreateCallHistory), new JsonResponse<CallHistory>(result));
        }
        [HttpDelete("api/callHistory/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> DeleteCallHistory([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            await _mediator.Send(new DeleteCallHistoryCommand(callid: id), cancellationToken);
            return Ok();
        }
        [HttpPut("api/callHistory")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdateCallHistory(
            
            [FromBody] UpdateCallHistoryCommand command,
            CancellationToken cancellationToken = default)
        {

            try
            {
                await _mediator.Send(command, cancellationToken);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while processing the request.");
            }
        }
    }
}