﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketRepositories : IEFRepository<Ticket, Ticket>
    {
        Task<Ticket?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByStatusIdAsync(int statusId, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<Ticket?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Ticket>> FindByCustomerIdAsync(int customerId, CancellationToken cancellationToken = default);
        Task<List<Ticket>> GetTicketSP(CancellationToken cancellationToken = default);
        Task<List<Ticket>> GetTicketByDateSP(DateTime date, CancellationToken cancellationToken = default);
        Task<List<Ticket>> GetTicketByIdSP(int id, CancellationToken cancellationToken = default);
    }
}
