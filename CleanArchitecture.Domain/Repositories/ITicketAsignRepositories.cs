﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketAsignRepositories : IEFRepository<TicketAsign, TicketAsign>
    {
        Task<TicketAsign?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<TicketAsign>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<TicketAsign>> GetTicketAsignSP(CancellationToken cancellationToken = default);
        Task<List<TicketAsign>> GetTicketAsignByIdSP(int id, CancellationToken cancellationToken = default);

    }
}