﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IBranchRepositories : IEFRepository<Branch, Branch>
    {
        Task<Branch?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<Branch>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<Branch?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Branch>> GetBranchSP(CancellationToken cancellationToken = default);
    }
}
