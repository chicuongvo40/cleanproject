﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ICustomerRepositories : IEFRepository<Customer, Customer>
    {
        Task<Customer?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<Customer>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<Customer>> FindAllCustomer(CancellationToken cancellationToken = default);
        Task<Customer?> FindByEmailAsync(string email, CancellationToken cancellationToken = default);
        Task<Customer?> FindByPhoneNumberAsync(string phoneNumber, CancellationToken cancellationToken = default);
        Task<List<Customer>> GetCustomerSP(CancellationToken cancellationToken = default);

        Task<List<Customer>> SearchCustomerSP (string name, CancellationToken cancellationToken = default);
        Task<List<Customer>> GetCustomerByIdSP(int id, CancellationToken cancellationToken = default);
    }
}
