﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IScheduleRepositories : IEFRepository<Schedule, Schedule>
    {
        Task<Schedule?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Schedule>> SearchByTittle(string tittle, CancellationToken cancellationToken);
        Task<List<Schedule>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}
