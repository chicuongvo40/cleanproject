﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketImageRepositories : IEFRepository<TicketImage, TicketImage>
    {
        Task<TicketImage?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<TicketImage>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<TicketImage>> GetTicketImageSP(CancellationToken cancellationToken = default);
        Task<List<TicketImage>> GetTicketImageByIdSP(int id, CancellationToken cancellationToken = default);

    }
}
