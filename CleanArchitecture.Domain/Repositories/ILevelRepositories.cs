﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ILevelRepositories : IEFRepository<Level, Level>
    {
        Task<Level?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<List<Level>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<Level?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<int> CreateLevelSP(Level op, CancellationToken cancellationToken = default);

        Task<List<Level>> GetLevelSP(CancellationToken cancellationToken = default);

        Task<int> UpdateLevelSP(Level op, CancellationToken cancellationToken = default);

        Task<int> DeleteLevelSP(int op, CancellationToken cancellationToken = default);
        Task<List<Level>> GetLevelByIdSP(int id, CancellationToken cancellationToken = default);
    }
}

