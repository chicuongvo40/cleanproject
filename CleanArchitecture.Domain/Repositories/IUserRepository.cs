﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface IUserRepository : IEFRepository<User, User>
    {
        Task<User?> FindByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<User?> Login(string username, string password, CancellationToken cancellationToken = default);
        Task<List<User>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
        Task<List<User>> FindByRoleIdAsync(int roleId, CancellationToken cancellationToken = default);
        Task<List<User>> GetUserSP(CancellationToken cancellationToken = default);
        Task<List<User>> GetUserByIdSP(int id, CancellationToken cancellationToken = default);
    }
}
