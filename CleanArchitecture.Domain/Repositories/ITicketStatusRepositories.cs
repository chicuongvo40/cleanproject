﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Repositories
{
    public interface ITicketStatusRepositories : IEFRepository<TicketStatus, TicketStatus>
    {
        Task<TicketStatus?> FindByIdAsync(int id, CancellationToken cancellationToken = default);

        Task<List<TicketStatus>> FindByIdsAsync(int ids, CancellationToken cancellationToken = default);
    }
}
