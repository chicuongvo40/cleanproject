﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class SourceDTO
    {
        public int Id { get; set; }

        public string? SourceName { get; set; }
    }
}
