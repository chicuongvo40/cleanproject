﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketDTO
    {
        public int Id { get; set; }

        public int? CallId { get; set; }

        public string? Note { get; set; }

        public int? CustomerId { get; set; }

        public int? TicketTypeId { get; set; }

        public int? LevelId { get; set; }

        public int? TicketStatusId { get; set; }

        public DateTime? CreatedBy { get; set; }
    }
}
