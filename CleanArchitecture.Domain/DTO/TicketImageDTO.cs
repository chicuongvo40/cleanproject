﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketImageDTO
    {
        public int Id { get; set; }

        public int? LogId { get; set; }

        public string? ImageUrl { get; set; }

    }
}
