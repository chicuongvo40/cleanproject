﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class CallHistoryDTO
    {
        public int Id { get; set; }

        public string? CallNumber { get; set; }

        public int? UserId { get; set; }

        public int? CustomerId { get; set; }

        public string? RealTimeCall { get; set; }

        public string? StatusCall { get; set; }

        public DateTime? DateCall { get; set; }

        public string? RecordLink { get; set; }

        public string? Direction { get; set; }

        public string? TotalTimeCall { get; set; }
    }
}
