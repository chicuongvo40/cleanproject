﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class OdsDTO
    {
        public int Id { get; set; }

        public string? OdsBranch { get; set; }

        public string? OdsPassword { get; set; }
    }
}
