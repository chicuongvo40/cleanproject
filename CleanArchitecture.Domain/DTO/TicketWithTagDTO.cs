﻿using CleanArchitecture.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.DTO
{
    public class TicketWithTagDTO
    {
        public int Id { get; set; }

        public string? CallId { get; set; }

        public string? Note { get; set; }

        public string? CustomerId { get; set; }

        public string? TicketTypeId { get; set; }

        public string? LevelId { get; set; }

        public string? TicketStatusId { get; set; }

        public DateTime? CreatedBy { get; set; }
        public List<string> Tags { get; set; }
    }
}
