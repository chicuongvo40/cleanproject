﻿namespace CleanArchitecture.Api.Models
{
    public class CallHistoryRequest
    {
        public string ServiceName { get; set; }
        public string AuthUser { get; set; }
        public string AuthKey { get; set; }
        public string TypeGet { get; set; }
        public int HideFWOut { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string IsSort { get; set; }
        public string CallNum { get; set; }
        public string ReceiveNum { get; set; }
        public string Key { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
