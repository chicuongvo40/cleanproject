﻿using CleanArchitecture.Domain.Entities;

namespace CleanArchitecture.Api.Models
{
    public class CallHistoryResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public int total { get; set; }
        public List<CallHistories> data { get; set; }
    }
}
