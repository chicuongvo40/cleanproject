﻿namespace CleanArchitecture.Api
{
    public class EmailRequest
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> ToAddresses { get; set; }
    }
}
