﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.ModelsRequest
{
    public class WebHookDTO
    {
        [JsonProperty("LID")]
        public string LID { get; set; }
        
        [JsonProperty("Dir")]
        public string? Dir { get; set; }
        
        [JsonProperty("CID")]
        public string? CID { get; set; }

        [JsonProperty("DID")]
        public string? DID { get; set; }

        [JsonProperty("TimeCall")]
        public string? TimeCall { get; set; }

        [JsonProperty("TimeDial")]
        public string? TimeDial { get; set; }

        [JsonProperty("Ch")]
        public string? Ch { get; set; }

        [JsonProperty("ChDest")]
        public string? ChDest { get; set; }

        [JsonProperty("Ext")]
        public string? Ext { get; set; }

        [JsonProperty("Uid")]
        public string? Uid { get; set; }

        [JsonProperty("recordingfile")]
        public string? recordingfile { get; set; }

        [JsonProperty("Event")]
        public string? Event { get; set; }
    }
}
