﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketImage
{
    public TicketImage()
    {
    }

    public TicketImage(int? logId, string? imageUrl)
    {
        LogId = logId;
        ImageUrl = imageUrl;
    }

    public int Id { get; set; }

    public int? LogId { get; set; }

    public string? ImageUrl { get; set; }

    public virtual Log? Log { get; set; }
}
