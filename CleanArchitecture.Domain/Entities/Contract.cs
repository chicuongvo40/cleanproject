﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Contract
{
    public Contract()
    {
    }

    public Contract(int customerId, string contractType, string termsAndConditions, DateTime timeStart, DateTime timeEnd, DateTime lastEditedTime)
    {
        CustomerId = customerId;
        ContractType = contractType;
        TermsAndConditions = termsAndConditions;
        TimeStart = timeStart;
        TimeEnd = timeEnd;
        LastEditedTime = lastEditedTime;
    }

    public int Id { get; set; }

    public int? CustomerId { get; set; }

    public string? ContractType { get; set; }

    public string? TermsAndConditions { get; set; }

    public DateTime? TimeStart { get; set; }

    public DateTime? TimeEnd { get; set; }

    public DateTime? LastEditedTime { get; set; }
    public virtual Customer? Customer { get; set; }
}
