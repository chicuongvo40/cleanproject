﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Customer
{
    public Customer()
    {
    }

    public Customer(int customerLevelId, int sourceId, int branchId, string lastName, string firstName, string phoneNumber, string status, string gender, string address, DateTime dayOfBirth, DateTime dateCreated, DateTime lastEditedTime, string name)
    {
        CustomerLevelId = customerLevelId;
        SourceId = sourceId;
        BranchId = branchId;
        LastName = lastName;
        FirstName = firstName;
        PhoneNumber = phoneNumber;
        Status = status;
        Gender = gender;
        Address = address;
        DayOfBirth = dayOfBirth;
        DateCreated = dateCreated;
        LastEditedTime = lastEditedTime;
        Name = name;
    }

    public int Id { get; set; }

    public int? CustomerLevelId { get; set; }

    public int? SourceId { get; set; }

    public int? BranchId { get; set; }

    public string? LastName { get; set; }

    public string? FirstName { get; set; }

    public string? PhoneNumber { get; set; }

    public string? Status { get; set; }

    public string? Gender { get; set; }

    public string? Address { get; set; }

    public DateTime? DayOfBirth { get; set; }

    public DateTime? DateCreated { get; set; }

    public DateTime? LastEditedTime { get; set; }

    public string? Name { get; set; }

    public virtual Branch? Branch { get; set; }
    public virtual ICollection<Contract> Contracts { get; set; } = new List<Contract>();
    public virtual ICollection<CallHistory> CallHistories { get; set; } = new List<CallHistory>();

    public virtual CustomerLevel? CustomerLevel { get; set; }

    public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

    public virtual Source? Source { get; set; }

    public virtual ICollection<Survey> Surveys { get; set; } = new List<Survey>();
}
