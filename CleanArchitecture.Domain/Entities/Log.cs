﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Log
{
    private string isAsigned;
    private string asignedTo;

    public Log()
    {
    }

    public Log(int ticketId, string note, DateTime createdDate, string isAssigned, string assignedTo, string reason, DateTime timeStart, DateTime timeEnd, int userId)
    {
        TicketId = ticketId;
        Note = note;
        CreatedDate = createdDate;
        IsAssigned = isAssigned;
        AssignedTo = assignedTo;
        Reason = reason;
        TimeStart = timeStart;
        TimeEnd = timeEnd;
        UserId = userId;
    }

    public int Id { get; set; }

    public int? TicketId { get; set; }

    public string? Note { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? IsAssigned { get; set; }

    public string? AssignedTo { get; set; }

    public string? Reason { get; set; }

    public DateTime? TimeStart { get; set; }

    public DateTime? TimeEnd { get; set; }

    public int? UserId { get; set; }

    public virtual Ticket? Ticket { get; set; }

    public virtual ICollection<TicketImage> TicketImages { get; set; } = new List<TicketImage>();

    public virtual User? User { get; set; }
}
