﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class User
{
    public User()
    {
    }

    public User(int branchId, int roleId, string lastName, string firstName, string userName, string password, string status, string gender, string address, DateTime dayOfBirth, DateTime createdDate)
    {
        BranchId = branchId;
        RoleId = roleId;
        LastName = lastName;
        FirstName = firstName;
        UserName = userName;
        Password = password;
        Status = status;
        Gender = gender;
        Address = address;
        DayOfBirth = dayOfBirth;
        CreatedDate = createdDate;
    }

    public int Id { get; set; }

    public int? BranchId { get; set; }

    public int? RoleId { get; set; }

    public string? LastName { get; set; }

    public string? FirstName { get; set; }

    public string? UserName { get; set; }

    public string? Password { get; set; }

    public string? Status { get; set; }

    public string? Gender { get; set; }

    public string? Address { get; set; }

    public DateTime? DayOfBirth { get; set; }

    public DateTime? CreatedDate { get; set; }

    public virtual Branch? Branch { get; set; }

    public virtual ICollection<CallHistory> CallHistories { get; set; } = new List<CallHistory>();

    public virtual ICollection<Log> Logs { get; set; } = new List<Log>();

    public virtual Role? Role { get; set; }

    public virtual ICollection<Schedule> Schedules { get; set; } = new List<Schedule>();

    public virtual ICollection<TicketAsign> TicketAsigns { get; set; } = new List<TicketAsign>();
}
