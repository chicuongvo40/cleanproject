﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Level
{
    public Level()
    {
    }

    public Level(string levelName)
    {
        LevelName = levelName;
    }

    public int Id { get; set; }

    public string? LevelName { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
