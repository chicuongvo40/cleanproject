﻿using CleanArchitecture.Domain.Repositories;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanArchitecture.Domain.Entities
{
    [EnableCors]
    public class ChatHub : Hub, IChatHub
    {
        public ChatHub()
        {

        }
        public async Task SendMessage(NotifyMessage message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }


    }
}
