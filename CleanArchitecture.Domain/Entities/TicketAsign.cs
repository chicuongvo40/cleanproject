﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketAsign
{
    public TicketAsign()
    {
    }

    public TicketAsign(int ticketId, DateTime timeStart, DateTime timeEnd, string status, int userId)
    {
        TicketId = ticketId;
        TimeStart = timeStart;
        TimeEnd = timeEnd;
        Status = status;
        UserId = userId;
    }

    public int Id { get; set; }

    public int? TicketId { get; set; }

    public DateTime? TimeStart { get; set; }

    public DateTime? TimeEnd { get; set; }

    public string? Status { get; set; }

    public int? UserId { get; set; }

    public virtual Ticket? Ticket { get; set; }

    public virtual User? User { get; set; }
}
