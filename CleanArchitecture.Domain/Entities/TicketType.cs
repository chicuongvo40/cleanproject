﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketType
{
    public TicketType()
    {
    }

    public TicketType(string ticketTypeName, string kpiDuration)
    {
        TicketTypeName = ticketTypeName;
        KpiDuration = kpiDuration;
    }

    public int Id { get; set; }

    public string? TicketTypeName { get; set; }

    public string? KpiDuration { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
