﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Ticket
{
    public Ticket() { }
    public Ticket(int callId, string note, int customerId, int ticketTypeId, int levelId, int ticketStatusId, DateTime createdBy)
    {
        CallId = callId;
        Note = note;
        CustomerId = customerId;
        TicketTypeId = ticketTypeId;
        LevelId = levelId;
        TicketStatusId = ticketStatusId;
        CreatedBy = createdBy;
    }
    public Ticket(int callId, string note, int customerId, int ticketTypeId, int levelId, int ticketStatusId, DateTime createdBy, List<Log> listlog)
    {
        CallId = callId;
        Note = note;
        CustomerId = customerId;
        TicketTypeId = ticketTypeId;
        LevelId = levelId;
        TicketStatusId = ticketStatusId;
        CreatedBy = createdBy;
        Logs = listlog;
    }
    public int Id { get; set; }

    public int CallId { get; set; }

    public string? Note { get; set; }

    public int CustomerId { get; set; }

    public int TicketTypeId { get; set; }

    public int LevelId { get; set; }

    public int TicketStatusId { get; set; }

    public DateTime? CreatedBy { get; set; }

    public virtual Level? Level { get; set; }

    public virtual ICollection<Log> Logs { get; set; } = new List<Log>();

    public virtual ICollection<TicketAsign> TicketAsigns { get; set; } = new List<TicketAsign>();

    public virtual TicketStatus? TicketStatus { get; set; }

    public virtual ICollection<TicketTagConnect> TicketTagConnects { get; set; } = new List<TicketTagConnect>();

    public virtual TicketType? TicketType { get; set; }
}
