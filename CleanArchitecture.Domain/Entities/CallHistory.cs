﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class CallHistory
{
    public CallHistory()
    {
    }

    public CallHistory(string callNumber, int userId, int customerId, string realTimeCall, string statusCall, DateTime dateCall, string recordLink, string direction, string totalTimeCall)
    {
        CallNumber = callNumber;
        UserId = userId;
        CustomerId = customerId;
        RealTimeCall = realTimeCall;
        StatusCall = statusCall;
        DateCall = dateCall;
        RecordLink = recordLink;
        Direction = direction;
        TotalTimeCall = totalTimeCall;
    }

    public int Id { get; set; }

    public string? CallNumber { get; set; }

    public int? UserId { get; set; }

    public int? CustomerId { get; set; }

    public string? RealTimeCall { get; set; }

    public string? StatusCall { get; set; }

    public DateTime? DateCall { get; set; }

    public string? RecordLink { get; set; }

    public string? Direction { get; set; }

    public string? TotalTimeCall { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual User? User { get; set; }
}
