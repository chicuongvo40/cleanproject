﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Branch
{
    public Branch()
    {
    }

    public Branch(int odsId, string address, string branchName)
    {
        OdsId = odsId;
        Address = address;
        BranchName = branchName;
    }

    public int Id { get; set; }

    public int? OdsId { get; set; }

    public string? Address { get; set; }

    public string? BranchName { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();

    public virtual Od? Ods { get; set; }

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
