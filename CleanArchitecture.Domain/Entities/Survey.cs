﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class Survey
{
    public Survey()
    {
    }

    public Survey(string satisfactionRating, string comment, DateTime surveyDate, int customerId)
    {
        SatisfactionRating = satisfactionRating;
        Comment = comment;
        SurveyDate = surveyDate;
        CustomerId = customerId;
    }

    public int Id { get; set; }

    public string? SatisfactionRating { get; set; }

    public string? Comment { get; set; }

    public DateTime? SurveyDate { get; set; }

    public int? CustomerId { get; set; }

    public virtual Customer? Customer { get; set; }
}
