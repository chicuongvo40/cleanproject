﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Entities;

public partial class TicketTag
{
    public int Id { get; set; }

    public string? TagName { get; set; }

    public virtual ICollection<TicketTagConnect> TicketTagConnects { get; set; } = new List<TicketTagConnect>();
}
